###### Fit Models ######

# This document contains a number of functions which will be used to fit to certain measured values.
# Further fit functions can be easily added.
# Below are a few helper function for the complex fit function expfun_dose() from the class RCFSTack
#
#

import numpy as np

########################################################
######
###### fit functions for the exponential spectrum
###### they need to be passed logarithmic as well
######
########################################################

def fuchs():
    # pure spectrum
    def f(x,params):
        FittedCurve =params[0]/np.sqrt(2*x*params[1])*np.exp(-np.sqrt(2*x/params[1]))
        return FittedCurve
    f.label = '(a/sqrt(2*E/b)) * exp(-sqrt(2E/b)) * H(-E + c)'
    f.nrOfParams = 3
    f.doi = 'https://doi.org/10.1038/nphys199'
    # log spectrum
    def f_log(x,params):
        FittedCurve = np.log(params[0]/np.sqrt(2*x*params[1])*np.exp(-np.sqrt(2*x/params[1])))
        return FittedCurve
    f.label = '(a/sqrt(2*E/b)) * exp(-sqrt(2E/b)) * H(-E + c)'
    f.nrOfParams = 3
    f.doi = 'https://doi.org/10.1038/nphys199'
    # dose weigthed with deposited energy -- minimization quantity
    def f_dose(params, ELO):
        P1 = params[0]  # to be optimized
        P2 = params[1]  # to be optimized
        P3 = params[2]  # to be optimized
        # dose function
        return P1 * 1e12 / np.sqrt(ELO[:, 0, :] * P2) * np.exp(-np.sqrt(2*ELO[:, 0, :]/P2)) * ELO[:, 3, :]
    f_dose.label = '(a/sqrt(2*E/b)) * exp(-sqrt(2E/b)) * H(-E + c)'
    f_dose.nrOfParams = 3
    f_dose.doi = 'https://doi.org/10.1038/nphys199'
    
    
    return f, f_dose, f_log

def expfun_fit():
    # pure spectrum
    def f(x,params):
        FittedCurve = params[0] / x * np.exp(-x / params[1])
        return FittedCurve
    f.label = 'a/E * exp(-E/b) * H(-E + c)'
    f.nrOfParams = 3
    # log spectrum 
    def f_log(x,params):
        FittedCurve = np.log(params[0] / x * np.exp(-x / params[1]))
        return FittedCurve
    f.label = 'a/E * exp(-E/b) * H(-E + c)'
    f.nrOfParams = 3
    # dose weigthed with deposited energy -- minimization quantity
    def f_dose(params, ELO):
        P1 = params[0]  # to be optimized
        P2 = params[1]  # to be optimized
        P3 = params[2]  # to be optimized
        # dose function
        return P1 * 1e12 / ELO[:, 0, :] * np.exp(-ELO[:, 0, :] / P2) * ELO[:, 3, :]
    f_dose.label = 'a/E * exp(-E/b) * H(-E + c)'
    f_dose.nrOfParams = 3
    
    return f, f_dose, f_log

def expfun2_fit():
    # pure spectrum
    def f(x,params):
        FittedCurve = params[0] / x * np.exp(-x**2 / params[1]**2)
        return FittedCurve
    f.label = 'a/E * exp(-E**2 / b**2) * H(-E + c)'
    f.nrOfParams = 3
    # log spectrum
    def f_log(x,params):
        FittedCurve = np.log(params[0] / x * np.exp(-x**2 / params[1]**2))
        return FittedCurve
    f.label = 'a/E * exp(-E**2 / b**2) * H(-E + c)'
    f.nrOfParams = 3
    # dose weigthed with deposited energy -- minimization quantity
    def f_dose(params, ELO):
        P1 = params[0]  # to be optimized
        P2 = params[1]  # to be optimized
        P3 = params[2]  # to be optimized
        # dose function
        return P1 * 1e12 / ELO[:, 0, :] * np.exp(-ELO[:, 0, :]**2 / P2**2) * ELO[:, 3, :]
    f_dose.label = 'a/E * exp(-E**2 / b**2) * H(-E + c)'
    f_dose.nrOfParams = 3
    
    return f, f_dose, f_log

####################################################
#####
##### fit functions for the angle divergence 
#####
####################################################

def parfun():
    def f(x,params):
        FittedCurve = params[0] * np.sqrt(-2* np.log(x/params[1])) * (x/params[1]) ** (1/params[2])
        return FittedCurve
    f.label = 'a * sqrt(-2 ln(E/b)) * (E/b)**(1/c)'
    f.nrOfParams = 3
    return f

def quadfun():
    def f(x,params):
        FittedCurve = params[0] + params[1]*x + params[2]*x**2
        return FittedCurve
    f.label = 'a + b*E + c*E**2'
    f.nrOfParams = 3
    return f

def linfun():
    def f(x,params):
        FittedCurve = params[0] + params[1] * x
        return FittedCurve
    f.label = 'a + b*E'
    f.nrOfParams = 2
    return f


# sum of squared estimate of errors, as optimization quantity:
def sse_func(params, *args):
    fitfunction = args[0]
    FittedCurve = fitfunction(args[1], params)
    ErrorVector = FittedCurve - args[2]
    sse = np.sum(ErrorVector ** 2)
    return sse

################
    # Helper functions for expfun_dose
################
def sumv(V):
    if(V.ndim == 1):
        return V
    else:
        s = np.sum(V, axis=0)
        return s

def simpsum_loc(y, DEL):
    if(y.ndim == 1):
        y = y.T
        N = len(y)
    if (y.ndim == 2):
        N,b = y.shape
    # if(len(DEL) == b):        DEL = P3 -> immer Skalar
    #     DEL = DEL.transpose()
    h = DEL/(N-1)
    if (np.mod(N, 2) == 0):
        s = np.sum(y, axis=0) - 5 / 8 * (y[0,:] + y[N-1,:])+(y[1,:] + y[N - 2,:]) / 6 - (y[2,:] + y[N -3,:]) / 24
        s = s * h
    else:
        s = (2 * sumv(y[2:N-2:2,:] ) + 4 * sumv(y[1:N-1:2,:]) +y[0] + y[N-1]) * h/3
    return s

def swapelement(vec,ind,val):
    #% swaps val as element ind, into the vector vec
    vec[ind] = val
    return vec

##################################################
def vec2mat(vec, n,m):
    #% forms the matrix M, such that M(i,j) = vec(i+j-1)
    grid = np.mgrid[0:n, 0:m]
    i = grid[0]
    j=grid[1]
    ind = i+j
    mat = vec[ind]
    if(n == 1):
        mat = np.transpose(mat)
    return mat
##################################################

def rombextrap (StepRatio,der_init,rombexpon):
    """
           % do romberg extrapolation for each estimate
        %
        %  StepRatio - Ratio decrease in step
        %  der_init - initial derivative estimates
        %  rombexpon - higher order terms to cancel using the romberg step
        %
        %  der_romb - derivative estimates returned
        %  errest - error estimates
        %  amp - noise amplification factor due to the romberg step

    """
    srinv = 1 / StepRatio

    #% do nothing if no romberg terms
    nexpon = len(rombexpon)
    rmat = np.ones((nexpon + 2, nexpon + 1))
    #% two romberg terms
    rmat[1, 1: 3] = srinv ** rombexpon
    rmat[2, 1: 3] = srinv ** (2 * rombexpon)
    rmat[3, 1: 3] = srinv **(3 * rombexpon)

    #% qr factorization used for the extrapolation as well
    #% as the uncertainty estimates
    qromb, rromb = np.linalg.qr(rmat)

    #% the noise amplification is further amplified by the Romberg step.
    # amp = cond(rromb);
    # this does the extrapolation to a zero step size.
    ne = len(der_init)
    rhs = vec2mat(der_init, nexpon + 2, ne - (nexpon + 2))

    q = np.transpose(np.conj(qromb)) @ rhs

    rombcoefs = np.linalg.lstsq(rromb , q, rcond=None)
    rombcoefs = rombcoefs[0]
    der_romb = np.transpose(rombcoefs[0,:])

    #% uncertainty estimate of derivative prediction
    s = np.sqrt(np.sum((rhs - rmat @ rombcoefs) ** 2, axis=0))
    rinv = np.linalg.lstsq( rromb , np.eye(nexpon + 1), rcond=None)
    rinv = rinv[0]
    cov1 = np.sum(rinv ** 2, axis=1)    #% 1 spare dof
    errest = np.transpose(s) *12.7062047361747*np.sqrt(cov1[0])

    return der_romb, errest

##########################################

def jacobianest(Emaxfit, x0, ELOarray1):

    nx = x0.size
    MaxStep = 100
    StepRatio = 2.0000001

    h, b, z = ELOarray1.shape
    for i in range(h):
        if (ELOarray1[i, 0, 0] == 0):
            ELOarray1[i, :, :] = 1e-5
    ELOarray1 = ELOarray1[ELOarray1[:, 0,0] <= Emaxfit+0.0001]

    y = np.squeeze(1e12 * x0[0] / ELOarray1[:, 0, :] * np.exp(-ELOarray1[:, 0, :] / x0[1]) * ELOarray1[:, 3, :])
    DEL = Emaxfit
    f0 = simpsum_loc(y, DEL)
    n = len(f0)
    if n == 0:
        # % empty begets empty
        jac = np.zeros((0, nx))
        err = jac
        return jac, err

    m = np.array(np.arange(0, -26, -1))
    relativedelta = MaxStep * StepRatio ** m
    nsteps = len(relativedelta)

    # % total number of derivatives we will need to take
    jac = np.zeros((n, nx))
    err = np.zeros((n, nx))

    for i in range(nx):
        x0_i = x0[i]
        if (not(x0_i == 0)):
            delta = x0_i * relativedelta
        else:
            delta = relativedelta

        fdel = np.zeros((n, nsteps), dtype=np.float64)
        # hier Abweichungen
        for j in range(nsteps):
            a = swapelement(x0, i, x0_i + delta[j])
            ya = np.squeeze(
                1e12 * a[0] / ELOarray1[:, 0, :] * np.exp(-ELOarray1[:, 0, :] / a[1]) * ELOarray1[:, 3, :])
            funa = simpsum_loc(ya, DEL)
            b = swapelement(x0, i, x0_i - delta[j])
            yb = np.squeeze(
                1e12 * b[0] / ELOarray1[:, 0, :] * np.exp(-ELOarray1[:, 0, :] / b[1]) * ELOarray1[:, 3, :])
            funb = simpsum_loc(yb, DEL)
            fdif = funa - funb

            fdel[:, j] = fdif[:]

        #  % these are pure second order estimates of the
        # % first derivative, for each trial delta.
        mat = np.array(0.5 / delta)
        repmat = np.tile(mat, (n, 1))
        derest = fdel * repmat
            #   % The error term on these estimates has a second order
            #   % component, but also some 4th and 6th order terms in it.
            #   % Use Romberg exrapolation to improve the estimates to
            #   % 6th order, as well as to provide the error estimate.
            #
            #   % loop here, as rombextrap coupled with the trimming
            #   % will get complicated otherwise.
        # rombe funktioniert
        for j in range(n):
            der_romb, errest = rombextrap(StepRatio, derest[j, :], np.array([2,4]))
            nest = len(der_romb)
            # % trim off 3 estimates at each end of the scale
            trim = np.array([0, 1, 2, nest - 3, nest - 2, nest - 1])
            tags = np.argsort(der_romb)
            der_romb = np.sort(der_romb)
            der_romb = np.delete(der_romb, trim)
            tags = np.delete(tags, trim)
            errest = errest[tags]

            #  % now pick the estimate with the lowest predicted error
            ind = np.argmin(errest)
            err[j, i] = np.min(errest)
            jac[j, i] = der_romb[ind]
    return jac, err
