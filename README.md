<!-- This file is part of pyRES code for the evaluation of RCFs.
It was written Technische Universitaet Darmstadt by Barbara Endl and Benedikt Schmitz, 
This source code is subject to the GNU General Public License version 3 and 
provided WITHOUT ANY WARRANTY. -->

# pythonic RCF Evaluation Software

[![License: Apache 2.0](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://www.apache.org/licenses/LICENSE-2.0)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6383868.svg)](https://doi.org/10.5281/zenodo.6383868)



## Table of contents
1. [Introduction](#introduction)
2. [Features](#features)
3. [How to Run our Code](#example)
4. [Structure of the Repository](#structure)
5. [Documentation](#doc)
6. [Known Bugs and Upcoming Features](#bugs)
7. [Publications & Preprints](#publications)
8. [Contributers](#contributors)
9. [Funding](#funding)

## 1. Introduction <a name="introduction"></a>

Usage is simplified using anaconda. The corresponding environment (image or image2) is attached as image.yaml.
pyRES is the python3 based evaluation software for radiochromic films (RCFs). 
It was developed at TEMF, TU Darmstadt as part of the research at the LOEWE center for Nuclear Photonics.

Its goal is an automatized evaluation of radiochromic films in laser plasma experiments.
This code summarizes several developments from the last decade in the field of dosimetry using radiochromic films and sets it up in a modern implementation using object orientation.
It also include a new approach for minimizing bias and evaluation time using image processing methods.
The methods are explained in a publication [1]. 

If you utilise our code as part of a publication, we would appreciate it if you cite it.
This code is currently at version 1.00. Additional features are planned, development and testing is ongoing.

## 2. Features <a name="features"></a>

Current key features include

* Model based and model free spectral evaluation 
* Possibility to apply own calibration easily if files are in the correct format (see documentation)
* Automatic segmentation of RCFs for ion species
* Background correction
* Spectral and spatial fits
* Several spatial and angle models implemented
* Easy extension to new models
* Additional bootstrapping tests of spectra
* Defined datatypes for export to later applications and documentation of the results including meta data

## 3. How to Run pyRES <a name="example"></a>

The image.yml describes the anaconda environment needed to run the code with all dependecies. 
Either this or an equivalent environment has to be used.

The jupyter notebook included in this repository guides the user then through an example evaluation process. 
ATTENTION: calibration files and data have to be supplied independently as given in the pyRES wiki (see documentation)


## 4. Structure of the Repository <a name="structure"></a>
The repository includes several .py files indicating the class they represent in the files name. 
fitfunctions.py is important for the model definitions. New models have to be insterted there. 
The image.yml describes the virtual python environment needed.
pyRES GUI.ipynb includes a step by step evaluation for a data set. 

## 5. Documentation <a name="doc"></a>
The documentation is done in the corresponding gitlab wiki.
Please check there if question arise and to check for the data types needed to pass to the evaluation.


## 6. Known Bugs and Upcoming Features <a name="bugs"></a>
For a list of known bugs and upcoming features, please have a look at 
the issue tracker on the gitlab.

## 7. Publications & Links <a name="publications"></a>
[1] [LOEWE center for Nuclear Photonics](https://www.ikp.tu-darmstadt.de/nuclearphotonics/nuclear_photonics/index.en.jsp) 

[2] [Accelerator Pyhsics at TEMF, TU Darmstadt](https://www.bp.tu-darmstadt.de/fachgebiet_beschleunigerphysik/index.en.jsp)

## 8. Contributors <a name="contributors"></a>
Contributors include (alphabetically): 
*   B. Endl, 
*   B. Schmitz

## 9. Funding <a name="funding"></a>
The work on pyRES was made possible by:
*  HMWK through the LOEWE centre ”Nuclear Photonics”.
