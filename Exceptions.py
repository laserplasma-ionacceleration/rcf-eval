class SaturationException(Exception):
    """
          A class to represent a Exception.
          See RCFImage.counts2OD
          see RCFImage.OD2MeV
    """
    pass