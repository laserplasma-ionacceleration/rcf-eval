class Target:
    """
       A class to represent the target.

       Attributes
       ----------
       name : str
           name of the target or notes to identify target
       thickness : int
           thickness of the target in micrometer
       distance : int
            distance between target and RCF in millimeter
       typ : str
            typ of the target (e.g. thinfoil, liquid, gas, structured ...)
       material : str
            material of the target (e.g. Gold/Au, Water/H2O)
       """
    thickness = 0
    distance = 0
    name = ""
    typ = ""
    material = ""

    def __init__(self, thickness, distance, name, typ, material):
        """
               Constructs all the necessary attributes for the Target object.

               Parameters
               ----------
                see attributes
               """

        self.thickness = thickness
        self.distance = distance
        self.name = name
        self.typ = typ
        self.material = material
