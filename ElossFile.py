import numpy as np
class ElossFile:
    """
       A class to represent the ElossFile.

       Attributes
       ----------
       name : str
           name of the elossfile
       filepath : str
            path to the file with the Eloss values as .csv
       elossfileValues : numpy.ndarray of floats with size (4300,4)
                        output of former rcf_energyloss_gui_master

       """

    def __init__(self, name, filepath):
        """
               Constructs all the necessary attributes for the ElossFile object from a .csv file

               Parameters
               ----------
                see attributes
               """
        self.name = name
        self.filepath = filepath
        self.elossfileValues = np.genfromtxt(self.filepath, delimiter=',', dtype=float)

