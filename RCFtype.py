class RCFtype:
    """
    A class to represent the RCF type.

    primitive Attributes
    ----------
    name : str
        name of the RCF type
    filmtype : str
        filmtype; used in later classes to identfy cases.
    scale : int
        scaling to convert dose to kev/mm^2/micron
        Default values:
            scaleHD = 1.08*1e-3/(10^3*10^3)/1.602176634e-16
            scaleEBT = 1.2*1e-3/(10^3*10^3)/1.602176634e-16

    non-primitive Attributes (Instance Variables, see Constructor):
    ----------
     a : numpy.ndarray of floats with size (3)
         Calibration fit parameter a for the [RGB] components.
     b : numpy.ndarray of floats with size (3)
         Calibration fit parameter b for the [RGB] components.
     c : numpy.ndarray of floats with size (3)
         Calibration fit parameter c for the [RGB] components.
     deltaa : numpy.ndarray of floats with size (3)
         1 sigma uncertainty for the fit parameter a for the [RGB] components.
     deltab : numpy.ndarray of floats with size (3)
         1 sigma uncertainty for the fit parameter b for the [RGB] components.
     deltac : numpy.ndarray of floats with size (3)
         1 sigma uncertainty for the fit parameter c for the [RGB] components.
     limesDfit : numpy.ndarray of floats with size (3)
         Limes for the derivative of the fit function (weighting)  for the [RGB] components.
     satt : numpy.ndarray of floats with size (3)
         Saturation value of the fit function for the [RGB] components.
     start : numpy.ndarray of floats with size (3)
         Start value of the dynamic range of the fit function for the [RGB] components.
     stop : numpy.ndarray of floats with size (3)
         Stop value of the dynamic range of the fit function for the [RGB] components.

     covR : numpy.ndarray of floats with size (3)
         Covariance values in [sigma_ab, sigma_ac, sigma_bc] for R component.
     covG : numpy.ndarray of floats with size (3)
         Covariance values in [sigma_ab, sigma_ac, sigma_bc] for G component.
     covB : numpy.ndarray of floats with size (3)
         Covariance values in [sigma_ab, sigma_ac, sigma_bc] for B component.

     RGBclean : numpy.ndarray of floats with size (3)
           negated average RGB values of the RCF film
     RGBsigma : numpy.ndarray of floats with size (3)
           sigma of the RGBclean values
     IRclean : numpy.ndarray of floats with size (1)
           negated average IR value of the RCF film
     sigmaIR : numpy.ndarray of floats with size (1)
          sigma of the IRclean value
     booleanb0: boolean value
          Switch to check which calibration function is used, either where b=0 (True), else b was variable (False)
     energy_sigma : float
          Uncertainty of the energyresolution caused by the finite Braggpeak width.

    """

    name = ""
    scale = 0

    def __init__(self, name, filmtype, scale, a,b,c, deltaa, deltab, deltac, covR, covG, covB, limesDfit, satt, start, stop, RGBclean, RGBsigma, IRclean, sigmaIR, booleanb0, energy_sigma):
        """
              Constructs all the necessary attributes for the RCF type object.

              Parameters
              ----------
               see attributes

              """
        self.name = name
        self.filmtype = filmtype
        self.scale = scale
        self.a = a
        self.b = b
        self.c = c
        self.deltaa = deltaa
        self.deltab = deltab
        self.deltac = deltac
        self.limesDfit = limesDfit
        self.satt = satt
        self.start = start
        self.stop = stop
        self.covR = covR
        self.covB = covB
        self.covG = covG
        self.RGBclean = RGBclean
        self.RGBsigma = RGBsigma
        self.IRclean = IRclean
        self.sigmaIR = sigmaIR
        self.booleanb0 = booleanb0
        self.energy_sigma = energy_sigma
        
    def load_Cal_from_pickle(self,file_path):
        """
        Construct an RCF Type object from external calibration files.
        Setup of these files are documented in the project wiki.

        Parameters
        ----------
        file_path : str
            Path to the calibration file.

        Returns
        -------
        New : RCFType
            The initialized RCFType object.

        """
        
        import numpy as np
        # import pickle and call file_path 
        # save serialized pickle data to imported_data dictionary
        import pickle
        with open(file_path, 'rb') as handle:
            imported_data = pickle.load(handle)
        
        # Preallocate empty variables
        a=[]
        b=[]
        c=[]
        deltaa = []
        deltab = []
        deltac = []
        cov = [] 
        limesDfit = []
        satt = []
        
        stop = []
        
        RGBclean = []
        RGBsigma = []
        
        # 
        for x in [imported_data['R'], imported_data['G'], imported_data['B']]:    
            a.append(x['fit_dosis_params'][0])
            b.append(x['fit_dosis_params'][1])
            c.append(x['fit_dosis_params'][2])
            deltaa.append(np.sqrt(x['fit_dosis_cov'][0,0]))
            deltab.append(np.sqrt(x['fit_dosis_cov'][1,1]))
            deltac.append(np.sqrt(x['fit_dosis_cov'][2,2]))
            # each list element consists of [cov_ab, cov_ac, cov_bc]
            cov.append(np.array([x['fit_dosis_cov'][0,1],x['fit_dosis_cov'][0,2],x['fit_dosis_cov'][1,2]])) 
        
        # Values extracted from Meta_data:
        Meta = imported_data['Meta']
        
        tmp = Meta['clean_RGB '].split(',')
        stmp= Meta['sigma_clean_RGB '].split(',')
        for i in [0,1,2]:
            RGBclean.append(int(tmp[i]))
            RGBsigma.append(int(stmp[i]))
            
        RGBclean=65534-np.array(RGBclean)
        RGBsigma=np.array(RGBsigma)
        IRclean=3,
        sigmaIR=None
        
        # limesDFit
        limesDfit = Meta['limesDFit']
        satt  = Meta['y_satt']
        start = Meta['start']
        stop  = Meta['stop']
        # stop value of the calibration
        ## stop can be chosen as inifinity, since the calibration range is limited with 
        ## the saturation value. 
        ## The saturation value "satt" is given by maximal calibrated energy or a
        ## numerical condition defined over the derivative, whichever is smaller.
        ## So the stop value of the range can be selected as infinity.
        
        # Extract RCF Types name for the scale factor
        filmtype = Meta['film_type '].split()[0] # split is necessary to get rid of unnecessary white space and to not use regular expressions
        thickness = int(Meta['thickness/micron '])
        
        # Extract the energy uncertainty:
        energy_sigma = float(Meta['energy_sigma '])
        
        # Comment for the scale attribute:
        # scale from dose to kev/mm^2/micron
        # then later multiply by layer thickness in micron
        # 1.08 g/cm^3 for HD and MD type active layer
        # 1.2 g/cm^3 for EBT type active layer
        if filmtype in ['HD','HD-V2','HD-v2','H2']:
            # scaleHD = 1.08*1e-3/(10**3*10**3)/1.602176634e-16
            scale = 1.08*1e-3/(10**3*10**3)/1.602176634e-16*thickness
        elif filmtype in ['EBT','EBT3','E3']:
            # scaleEBT = 1.2*1e-3/(10**3*10**3)/1.602176634e-16    
            scale = 1.2*1e-3/(10**3*10**3)/1.602176634e-16*thickness
            
        if np.array(b).all() == 0:
            booleanb0 = True
        else:
            booleanb0 = False

        # Now create a new RCFtype object with parameters calculated above.
        New = RCFtype(name=filmtype,
                     filmtype = filmtype,
                     scale=scale,
                     a=np.array(a),
                     b=np.array(b),
                     c=np.array(c),
                     deltaa=np.array(deltaa),
                     deltab=np.array(deltab),
                     deltac=np.array(deltac),
                     covR = cov[0],
                     covG = cov[1],
                     covB = cov[2],
                     limesDfit=limesDfit,
                     satt=satt,
                     start=start,
                     stop=stop,
                     RGBclean=RGBclean,
                     RGBsigma=RGBsigma,
                     IRclean=IRclean,
                     sigmaIR=sigmaIR,
                     booleanb0=booleanb0,
                     energy_sigma=energy_sigma
                     )
        # pass object back up.
        return New


