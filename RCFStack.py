import numpy as np
import matplotlib.pyplot as plt
import cv2
import scipy.optimize
import math
import fitfunctions
import scipy.integrate
import pandas as pd

from fitfunctions import expfun_fit, expfun2_fit, parfun, expfun_fit, jacobianest


# helper functions
def uncertainty_data(x,y,sigma_x,sigma_y,number_of_samples):
    """
    Function to create additional data for evaluation.
    Method according to GUM manual.
    Random point resambling in x and y.

    Parameters
    ----------
    x : 1d array / list
        x value of dataset.
    y : 1d array / list
        y value of dataset.
    sigma_x : 1d array / list
        sigma value for x.
    sigma_y : 1d array / list
        sigma value for y.
    number_of_samples : integer
        Number of samples to be created.

    Returns
    -------
    additional_data_x : (len(x), number_of_samples) array
        Full resampled dataset for x.
    additional_data_y : (len(x), number_of_samples) array
        Full resampled dataset for y.

    """
    # if len(x) != len(y) != len(sigma_x) != len(sigma_y):
    #     print('Input vector lengths have to be the same.')
      
    number_of_points = len(y)
    
    
    # Uncertainties have to be evaluated using a monte carlo method. 
    # Several datapoints in the x\pm \sigma x range are calculated (followin gauss...)
    # Then fits are done and descriptional statistics is applied on the parameters.
    rng = np.random.default_rng(12345)
    
    if x is not None:
        # save resampled x-data
        additional_data_x = np.zeros((number_of_points, number_of_samples+1))
    if y is not None:
        # save resampled y-data
        additional_data_y = np.zeros((number_of_points, number_of_samples+1))

    for i in range(number_of_points):
        if x is not None:
            mean_x = x[i]
            sx= sigma_x[i]
            additional_data_x[i,0:number_of_samples] = rng.normal(loc=mean_x,scale=sx, size=number_of_samples)
        if y is not None:
            # same for y
            mean_y = y[i]
            sy= sigma_y[i]
            additional_data_y[i,0:number_of_samples] = rng.normal(loc=mean_y,scale=sy, size=number_of_samples)
    result = []
    if x is not None:
        additional_data_x[0:number_of_points,number_of_samples] = x
        result.append(additional_data_x)
    if y is not None:
         additional_data_y[0:number_of_points,number_of_samples] = y
         result.append(additional_data_y)
    
    return result


#### Ellipsis fit
def fit_ellipse(x, y):
    """
    Fit an ellipsis to the x and y coordinates.

    Parameters
    ----------
    x : 1d ndarray / list
        x value of dataset.
    y : 1d ndarray / list
        y value of dataset.

    Returns
    -------
    ellipse_coefficients : 1d ndarray  
        The coefficients of the fitted ellipsis.

    """
    
    """

    Fit the coefficients a,b,c,d,e,f, representing an ellipse described by
    the formula F(x,y) = ax^2 + bxy + cy^2 + dx + ey + f = 0 to the provided
    arrays of data points x=[x1, x2, ..., xn] and y=[y1, y2, ..., yn].

    Based on the algorithm of Halir and Flusser, "Numerically stable direct
    least squares fitting of ellipses".


    """

    D1 = np.vstack([x**2, x*y, y**2]).T
    D2 = np.vstack([x, y, np.ones(len(x))]).T
    S1 = D1.T @ D1
    S2 = D1.T @ D2
    S3 = D2.T @ D2
    T = -np.linalg.inv(S3) @ S2.T
    M = S1 + S2 @ T
    C = np.array(((0, 0, 2), (0, -1, 0), (2, 0, 0)), dtype=float)
    M = np.linalg.inv(C) @ M
    eigval, eigvec = np.linalg.eig(M)
    con = 4 * eigvec[0]* eigvec[2] - eigvec[1]**2
    ak = eigvec[:, np.nonzero(con > 0)[0]]
    return np.concatenate((ak, T @ ak)).ravel()


def cart_to_pol(coeffs):
    """
    Function to convert the cartesian coefficients of the fit (output from fit_ellipsis)
    to the ellipsis parameters.

    Parameters
    ----------
    coeffs : ndarray
        coefficients returned by fit_ellipsis.

    Raises
    ------
    ValueError
        Error if parameters do not describe an ellipsis.

    Returns
    -------
    x0 : float
        x-center of the ellipsis.
    y0 : float
        x-center of the ellipsis.
    ap : float
        Semi-major axis.
    bp : float
        Semi-minor axis.
    e : float
        Eccentricity of the ellipsis.
    phi : floar
        Rotation of the ellipsis, relative to the x-axis.

    """
    """
    Convert the cartesian conic coefficients, (a, b, c, d, e, f), to the
    ellipse parameters, where F(x, y) = ax^2 + bxy + cy^2 + dx + ey + f = 0.
    The returned parameters are x0, y0, ap, bp, e, phi, where (x0, y0) is the
    ellipse centre; (ap, bp) are the semi-major and semi-minor axes,
    respectively; e is the eccentricity; and phi is the rotation of the semi-
    major axis from the x-axis.

    """

    # We use the formulas from https://mathworld.wolfram.com/Ellipse.html
    # which assumes a cartesian form ax^2 + 2bxy + cy^2 + 2dx + 2fy + g = 0.
    # Therefore, rename and scale b, d and f appropriately.
    a = coeffs[0]
    b = coeffs[1] / 2
    c = coeffs[2]
    d = coeffs[3] / 2
    f = coeffs[4] / 2
    g = coeffs[5]

    den = b**2 - a*c
    if den > 0:
        raise ValueError('coeffs do not represent an ellipse: b^2 - 4ac must'
                         ' be negative!')

    # The location of the ellipse centre.
    x0, y0 = (c*d - b*f) / den, (a*f - b*d) / den

    num = 2 * (a*f**2 + c*d**2 + g*b**2 - 2*b*d*f - a*c*g)
    fac = np.sqrt((a - c)**2 + 4*b**2)
    # The semi-major and semi-minor axis lengths (these are not sorted).
    ap = np.sqrt(num / den / (fac - a - c))
    bp = np.sqrt(num / den / (-fac - a - c))

    # Sort the semi-major and semi-minor axis lengths but keep track of
    # the original relative magnitudes of width and height.
    width_gt_height = True
    if ap < bp:
        width_gt_height = False
        ap, bp = bp, ap

    # The eccentricity.
    r = (bp/ap)**2
    if r > 1:
        r = 1/r
    e = np.sqrt(1 - r)

    # The angle of anticlockwise rotation of the major-axis from x-axis.
    if b == 0:
        phi = 0 if a < c else np.pi/2
    else:
        phi = np.arctan((2.*b) / (a - c)) / 2
        if a > c:
            phi += np.pi/2
    if not width_gt_height:
        # Ensure that phi is the angle to rotate to the semi-major axis.
        phi += np.pi/2
    phi = phi % np.pi

    return x0, y0, ap, bp, e, phi


def get_ellipse_pts(params, npts=100, tmin=0, tmax=2*np.pi):
    """
    

    Parameters
    ----------
    params : list of floats
        Ellipsis parameters [x0, y0, ap, bp, e, phi]. Also the output of cart_to_pol.
    npts : int, optional
        Number of points calculated on the given ellipsis. The default is 100.
    tmin : float, optional
        Minimal parametric t value for the displayed ellipsis. The default is 0.
    tmax : float, optional
        Maximal parametric t value for the displayed ellipsis.. The default is 2*np.pi.

    Returns
    -------
    x : ndarray float
        npts number of points on the ellipsis.
    y : ndarray float
        npts number of points on the ellipsis.

    """
    """
    Return npts points on the ellipse described by the params = x0, y0, ap,
    bp, e, phi for values of the parametric variable t between tmin and tmax.

    """

    x0, y0, ap, bp, e, phi = params
    # A grid of the parametric variable, t.
    t = np.linspace(tmin, tmax, npts)
    x = x0 + ap * np.cos(t) * np.cos(phi) - bp * np.sin(t) * np.sin(phi)
    y = y0 + ap * np.cos(t) * np.sin(phi) + bp * np.sin(t) * np.cos(phi)
    return x, y



class RCFStack:
    """
    A class to represent a Stack of RCFs.
    The order of the individually RCFs is important

    primitive Attributes
    ----------
    name : str
        name of the RCF stack
    savepath : str
        path to the RCF stack
    numberOfRCFs : int
         number of RCFs in the stack
    N_0 : float
              fitparameter
    k_T : float
              fitparameter


    non-primitive Attributes (Instance Variables, see Constructor):
      ----------
    rcf_list: RCFImage
          RCFs in the Stack as RCFImage objects in the order of the stack construction

    laser_instance : Laser
          used laser as Laser object

    target_instance : Target
          used target as Target object

    scanner_instance : Scanner
          used scanner as Scanner object

    ELOarray : numpy.ndarray of floats with size (size ,4,numberOfRCFs)
              a map with energy loss per RCF

    current_dose_fitted : numpy.ndarray of floats with size numberOfRCFs
              the currently computed dose while the fitting algorithm during method fitfun_dose()

    sseJ : float
              the computed sum of the squared differences while the fitting algorithm during method fitfun_dose()

    ErrorVector : numpy.ndarray of floats with size numberOfRCFs
              the computed error during the fitting algorithm in method fitfun_dose()
    """


    savepath =""
    name = ""
    numberOfRCFs = 0
    N_0 = 0     # will be filled with a start value during the function simple fit
    k_T = 0     # will be filled with a start value during the function simple fit

    def __init__(self, numberOfRCFs, rcf_images, savepath, laser_instance, target_instance, scanner_instance, name):
        """
                    Constructs all the necessary attributes for the RCF Stack object.

                    Parameters
                    ----------
                     see attributes

                    """
        self.numberOfRCFs = numberOfRCFs
        self.savepath = savepath
        self.name = name
        # All RCFs in the Stack as list in the order of the stack construction
        self.rcf_list = rcf_images
        self.laser = laser_instance
        self.target = target_instance
        self.scanner= scanner_instance
        self.current_dose_fitted = None     # will be used during the fitting algorithm
        self.sseJ = 0                       # will be used during the fitting algorithm
        self.ErrorVector =None              # will be used during the fitting algorithm
        self.ELOarray = None                # will be filled with values during the function create_ELOarray
        # The following parameters save a dictionary with the numerical fitparameters and models.
        self.fit_results = {}
        self.spectrum_df = None
        self.angle_df = None
        self.angle_df_ellipsis = None
        self.modelfree_df = None
        self.modelfree_df_with_uncertainty = None


    def create_ELOarray(self):
        """
           This function fills the empty Attribute ELOarray with a 3D Array containing the Elossfiles from the RCF Images in the Stack
           The Result will be stored in Attribute ELOarray.
           """
        shape = self.rcf_list[0].elossfile.elossfileValues.shape
        ELOarray = np.zeros((shape[0], shape[1], self.numberOfRCFs))
        for i in range(self.numberOfRCFs):
            ELOarray[:, :, i] = self.rcf_list[i].elossfile.elossfileValues

        self.ELOarray = ELOarray

        if (shape[1] != 4):
            print("Achtung! Fehlt die Quench-Korrektur?")


    def plot_envelope_divergence(self, fitfunction, style, error_sampling=1000):
        """
        Shows and calculates the envelope of the divergence.

         Parameters
         ----------
          fitfunction: (adress of a) py-function
               the function which is used during the fitting algorithm. Most of the functions will be found in fitfunctions.py


          style: String
               shows the model which is used during the fitting algorithm.
               currently added: absolutediv, particlesdiv, normalizeddiv


         Plots
         ________
           The envelope half opening angle dependent on the proton energy (MeV)

         """
        level = 0.001
        sse_func = fitfunctions.sse_func        # sse_func computes the sse with a given function

        # initialize Parameters
        energies = np.zeros(self.numberOfRCFs)
        energies_error = np.zeros(self.numberOfRCFs)
        angle_deg = np.zeros(self.numberOfRCFs)
        error_angle_deg = np.zeros(self.numberOfRCFs)

        for i in range(self.numberOfRCFs):
            peaks_position = np.argmax(self.ELOarray[:, 3, i], axis=0)
            energies[i] = self.ELOarray[peaks_position, 0, i]

            # find Boundarys of binary mask (= outlines of radiated area)
            img8 = self.rcf_list[i].ImageMask
            # find longest contour in binary image to detect the outline of the beam and compute the wides distance in x and y direction
            img8 = img8.astype('uint8')
            # Only for debugging:  show binary mask
            # plt.imshow(img8)
            # plt.show()
            ret, thresh = cv2.threshold(img8, 0, 255, 0)
            contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            contours = sorted(contours, key=cv2.contourArea)
            # Only for debugging:  show contour
            #c = cv2.drawContours(img8, contours[-1], -1, (255, 255, 255), 3)
            # plt.imshow(c)
            # plt.show()
            longest_contour = contours[-1]
            l1 = longest_contour[:, 0, 1]
            l2 = longest_contour[:, 0, 0]

            D1 = np.max(l1) - np.min(l1)
            D2 = np.max(l2) - np.min(l2)
            
            # compute Error
            energies_error[i] = self.rcf_list[i].RCFType.energy_sigma
            
            # Assume circle for the cutting plane
            radius = ((D1 + D2) / 2) / 2
            radius_mm = radius * 25.4 / self.rcf_list[i].XResolution

            max_deviation = np.max([D1 / 2 - radius, D2 / 2 - radius])
            max_deviation_mm = max_deviation * 25.4 / self.rcf_list[i].XResolution

            # calculates the angle
            angle_rad = math.atan(radius_mm / self.target.distance)
            angle_deg[i] = angle_rad / (2 * math.pi) * 360
            error_angle_deg[i] = max_deviation_mm / self.target.distance / (2 * math.pi) * 360
        
        # uncertainty calculation is done with the bootstrapping approach:
        # Step 1 Resample Data:
        Sampled_Data = uncertainty_data(x=energies,y=angle_deg,sigma_x=energies_error,sigma_y=error_angle_deg,number_of_samples=error_sampling)
        Sampled_Result = np.zeros((len(Sampled_Data[0][1]), fitfunction.nrOfParams))
        # do fits for all data sets 
        for i in range(error_sampling+1):
            energies = Sampled_Data[0][:,i]
            angle_deg = Sampled_Data[1][:,i]
            
        
            # fit curve to data; different models
            if (style == "normalizeddiv"):
                xopt = scipy.optimize.fmin(func=sse_func, args=(fitfunction, energies / np.max(energies), angle_deg), x0=np.array([np.max(angle_deg), 1, 4]), disp = 0)
                x_fit = np.arange(0, 1, 0.01)
            else:
                xopt = scipy.optimize.fmin(func=sse_func, args=(fitfunction, energies, angle_deg), x0=np.array([np.max(angle_deg), np.max(energies), 4]), disp = 0)
                x_fit = np.arange(np.min(energies), np.max(energies) + 0.01, 0.01)
            
            Sampled_Result[i,:] = xopt
            
        # descriptive statistics on the sampled results:
        meanValue = np.mean(Sampled_Result,axis=0)
        covariance = np.cov(Sampled_Result,rowvar=False)
        
        # reassign mean to xopt
        xopt = meanValue
        
        # get fitted curve values for plotting
        y_fit = fitfunction(x_fit, xopt)
        
        print('OptsParams', xopt)

        if len(xopt) == 1:
            text = str('Angle(E) = '+fitfunction.label + '\n with a='+"{:5.2f}".format(xopt[0]))
        elif len(xopt) == 2:
            text = str('Angle(E) = '+fitfunction.label + '\n with a='+"{:5.2f}".format(xopt[0])+', b='+"{:5.2f}".format(xopt[1]))
        elif len(xopt) == 3:
            text = str('Angle(E) = '+fitfunction.label + '\n with a='+"{:5.2f}".format(xopt[0])+', b='+"{:5.2f}".format(xopt[1])+' c='+"{:5.2f}".format(xopt[2]))
        else: 
            text = str('Angle(E) = '+fitfunction.label)

        # Plot
        fig, ax = plt.subplots()
        if (style == "normalizeddiv"):
            ax.errorbar(energies / np.max(energies), angle_deg, xerr=energies_error / np.max(energies),
                        yerr=error_angle_deg, fmt='o', capsize=3, capthick=0.5, ecolor='black')
            ax.set_xlim([0, 1])
            ax.set_ylim([0, math.ceil(np.max(angle_deg)) + 5])
        else:
            ax.errorbar(energies, angle_deg, xerr=energies_error, yerr=error_angle_deg, fmt='o', capsize=5,
                        capthick=1, ecolor='black')
            ax.set_xlim([0, math.ceil(np.max(energies)) + 4])
            ax.set_ylim([0, math.ceil(np.max(angle_deg)) + 5])
        ax.plot(x_fit, y_fit, "r")
        ax.set(xlabel='proton energy (MeV)', ylabel='envelope half opening angle (deg)', title=text)
        plt.show()
        
        # save data to dataframe.
        data_angle = {}
        data_angle['Energy / MeV'] = energies
        data_angle['sigma Energy / MeV'] = energies_error
        data_angle['Half Angle / deg'] = angle_deg
        data_angle[' sigma Half Angle / deg'] = error_angle_deg
        self.angle_df = pd.DataFrame(data_angle)
        
        # save fit parameters
        self.fit_results['angle_fit'] = {'fit_func': fitfunction.label, 
                                          'params' : xopt,
                                          'covariance' :covariance}
        
        def plot_envelope_divergence_ellipse(self, fitfunction, style):
            """
            shows the envelope of the divergence calculated with an ellipsis.
            Attention: Not working yet. 
            how to deal with ellipsis? 

             Parameters
             ----------
              fitfunction: (adress of a) py-function
                   the function which is used during the fitting algorithm. Most of the functions will be found in fitfunctions.py


              style: String
                   shows the model which is used during the fitting algorithm.
                   currently added: absolutediv, particlesdiv, normalizeddiv


             Plots
             ________
               The envelope half opening angle dependent on the proton energy (MeV)

             """
            level = 0.001
            sse_func = fitfunctions.sse_func        # sse_func computes the sse with a given function

            # initialize Parameters
            energies = np.zeros(self.numberOfRCFs)
            energies_error = np.zeros(self.numberOfRCFs)
            angle_deg = np.zeros(self.numberOfRCFs)
            error_angle_deg = np.zeros(self.numberOfRCFs)
            
            # 
            x0s  = []
            y0s  = []
            aps  = [] 
            bps  = [] 
            es   = [] 
            phis = []

            for i in range(self.numberOfRCFs):
                peaks_position = np.argmax(self.ELOarray[:, 3, i], axis=0)
                energies[i] = self.ELOarray[peaks_position, 0, i]

                # find Boundarys of binary mask (= outlines of radiated area)
                img8 = self.rcf_list[i].ImageMask
                # find longest contour in binary image to detect the outline of the beam and compute the wides distance in x and y direction
                img8 = img8.astype('uint8')
                # Only for debugging:  show binary mask
                # plt.imshow(img8)
                # plt.show()
                ret, thresh = cv2.threshold(img8, 0, 255, 0)
                contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
                contours = sorted(contours, key=cv2.contourArea)
                # Only for debugging:  show contour
                #c = cv2.drawContours(img8, contours[-1], -1, (255, 255, 255), 3)
                # plt.imshow(c)
                # plt.show()
                longest_contour = contours[-1]
                l1 = longest_contour[:, 0, 1] # x component
                l2 = longest_contour[:, 0, 0] # y component
                
                coeffs = fit_ellipse(l1, l2) # fit ellipsis 
                x0, y0, ap, bp, e, phi = cart_to_pol(coeffs) # convert ellipsis parameters to cartesian
                
                # compute energy Error
                energies_error[i] = self.RCFType.energy_sigma
                
                # convert ellipsis radii
                x01 = x0 * 25.4 / self.rcf_list[i].XResolution
                y01 = y0 * 25.4 / self.rcf_list[i].XResolution
                ap1 = ap * 25.4 / self.rcf_list[i].XResolution
                bp1 = bp * 25.4 / self.rcf_list[i].XResolution
                
                
                # append layer values to list
                x0s.append(x01)
                y0s.append(y01)
                aps.append(ap1)
                bps.append(bp1)
                es.append(e)
                phis.append(phi)
                
                # radius = ((D1 + D2) / 2) / 2
                # radius_mm = radius * 25.4 / self.rcf_list[i].XResolution

                # max_deviation = np.max([D1 / 2 - radius, D2 / 2 - radius])
                # max_deviation_mm = max_deviation * 25.4 / self.rcf_list[i].XResolution

                # # calculates the angle
                # angle_rad = math.atan(radius_mm / self.target.distance)
                # angle_deg[i] = angle_rad / (2 * math.pi) * 360
                # error_angle_deg[i] = max_deviation_mm / self.target.distance / (2 * math.pi) * 360
            
            # save data to dataframe
            data_angle = {}
            data_angle['Energy / MeV'] = energies
            data_angle['sigma Energy / MeV'] = energies_error
            data_angle['x0 / mm'] = x0s
            data_angle['y0 / mm'] = y0s
            data_angle['ap / mm'] = aps
            data_angle['bp / mm'] = bps
            data_angle['eccentricity'] = es
            data_angle['phi'] = phis
            
            self.angle_df_ellipsis = pd.DataFrame(data_angle)


    def simplefit(self, boolean_print=True, fit_model=fitfunctions.expfun_fit()):
        '''
         This function calculates the Dose and fits a simple e-function to this data points.
         The parameter of this simple fit are the start parameter for more complex fitting algorithms like expfundose()

         The result is stored in attribute  N_0 and  k_T
        '''

        last = self.numberOfRCFs

        # Dose: Sum Array with all RCF-images in stack
        ImageArrays = np.empty([self.rcf_list[0].height, self.rcf_list[0].width, last])
        for i in range(last):
            ImageArrays[:,:,i]= self.rcf_list[i].MeV_map_cleaned
        dose = np.squeeze(np.sum(np.sum(ImageArrays, axis=1), axis=0))

        Elaser = self.laser.energy

        N_start = 1e10
        T_start = 1

        E = np.zeros(last)
        E_error = np.zeros(last)
        spectrum = np.zeros((last, 2))

        for frame in range(last):
            peaks_position = np.argmax(self.ELOarray[:, 3, frame])
            spectrum[frame, 1] = dose[frame] / np.sum(self.ELOarray[peaks_position, 3, frame], axis=0)
            E[frame] = np.mean(self.ELOarray[peaks_position, 0, frame])
            E_error[frame] = np.max(np.abs(self.ELOarray[peaks_position, 0, frame] - E[frame]))
            spectrum[frame, 0] = E[frame]

        xdata = spectrum[:, 0]
        ydata = spectrum[:, 1]
        logydata = np.log(ydata)#dose#ydata 

        # fit with simple function 
        func1 = fit_model[2]

        # minimize SSE
        xopt = scipy.optimize.fmin(func=fitfunctions.sse_func, args=(func1, xdata, logydata), x0=[N_start, T_start])
        param1 = xopt[0]
        param2 = xopt[1]
        xfit = np.arange(math.ceil(np.min(xdata)), math.ceil(np.max(xdata)) + 0.1, 0.1)
        # FittedCurve_simple = np.log(param1 / xfit * np.exp(-xfit / param2))
        FittedCurve_simple = fit_model[2](xfit,[param1,param2])

        # plot estimated curve
        if boolean_print:
            fig, ax = plt.subplots()
            ax.plot(xfit, np.exp(FittedCurve_simple), "k--")
            ax.plot(xdata, ydata, "rx")
            plt.title("fit simple. log (param1 / x * e^(-x/param2)), \n Parameter: " + str(param1) + ", " + str(param2))
            plt.show()

        # calculate conversion efficiency for protons > 4 MeV (threshold as in published papers)
        # summarize over all RCFs
        Etotal = 0

        if (last != 0):
            intervalpos = np.zeros((last))
            intervalneg = np.zeros((last))
            for i in range(last - 1, -1, -1):
                if (i != len(E) - 1):
                    intervalpos[i] = np.abs(0.5 * (xdata[i + 1] - xdata[i]))
                else:
                    intervalpos[i] = np.abs(0.5 * (xdata[i] - xdata[i - 1]))
                if (i != 0):
                    intervalneg[i] = np.abs(0.5 * (xdata[i] - xdata[i - 1]))
                else:
                    intervalneg[i] = abs(0.5 * (xdata[i + 1] - xdata[i]))

                Ecurrent = ydata[i] * (intervalneg[i] + intervalpos[i]) * xdata[i]

                if (xdata[i] >= 4):
                    Etotal = Etotal + Ecurrent

        Eproton = Etotal * 1e6 * 1.602e-19
        conversion = Eproton / Elaser * 100

        xfit = np.arange(math.ceil(np.min(xdata)), math.ceil(np.max(xdata)) + 0.1, 0.1)
        # FittedCurve = np.log(param1 / xfit * np.exp(-xfit / param2))
        FittedCurve = fit_model[2](xfit,[param1,param2])

        if boolean_print:
            fig, ax = plt.subplots()
            ax.plot(xfit, np.exp(FittedCurve), "k--")
    
            ax.errorbar(xdata, ydata, xerr=[intervalneg, intervalpos], yerr=0.5 * ydata, fmt='o', ecolor='r')
            ax.set_xlim([np.min(xdata) - 1, np.max(xdata) + 2])
            ax.set_ylim([np.min(ydata) - 0.8 * np.min(ydata), np.max(ydata) + 0.8 * np.max(ydata)])
            plt.yscale("log")
            ax.set(xlabel='E / MeV', ylabel='dN/dE / 1 $MeV^{-1}$',
                   title='Check for proper peak auto-detection', 
                   yscale=("log"))
            
            publish=False
            doseplot = False
            if publish == True:
                fig, ax = plt.subplots()
                ax.plot(xfit, np.exp(FittedCurve), "k--")
        
                ax.errorbar(xdata, ydata, xerr=[intervalneg, intervalpos], yerr=0.5 * ydata, fmt='o', ecolor='r')
                ax.set_xlim([np.min(xdata) - 1, np.max(xdata) + 2])
                ax.set_ylim([np.min(ydata) - 0.8 * np.min(ydata), np.max(ydata) + 0.8 * np.max(ydata)])
                plt.yscale("log")
                ax.xaxis.set_tick_params(labelsize=13)
                ax.yaxis.set_tick_params(labelsize=13)
                plt.xlabel('E / MeV', fontsize = 15, weight='bold')
                plt.ylabel('dN/dE / 1 MeV$^{-1}$', fontsize = 15, weight='bold')
        
            if doseplot==True:# plot for dose
                fig, ax = plt.subplots()
                ax.plot(xfit, np.exp(FittedCurve), "k--", label='$N_0$ =  ' + "{:.2e}".format(param1)+'\n'  'kT = : ' + "{:5.2f}".format(param2) + ' MeV ')
                
                ax.errorbar(xdata, dose, xerr=[intervalneg, intervalpos], yerr=0.5 * dose, fmt='o', ecolor='r')
                ax.set_xlim([np.min(xdata) - 1, np.max(xdata) + 2])
                ax.set_ylim([np.min(dose) - 0.8 * np.min(dose), np.max(dose) + 0.8 * np.max(dose)])
                plt.yscale("log")
                ax.xaxis.set_tick_params(labelsize=13)
                ax.yaxis.set_tick_params(labelsize=13)
                plt.xlabel('E / MeV', fontsize = 15, weight='bold')
                plt.ylabel('Dose / MeV', fontsize = 15, weight='bold')
                plt.legend()

            # # Annotations:
            # text0 = 'exp. fit: dN/dE = $N_0$  * exp(-E/kT)'
            # text1 = '$N_0$ =  ' + "{:.2e}".format(param1) + '\n'
            # text2 = 'kT = : ' + "{:5.2f}".format(param2) + ' MeV \n'
            # text3 = 'Laser energy: ' + "{:5.1f}".format(Elaser) + ' J \n'
            # text4 = 'Laser to proton energy (> 4 MeV) ' + '\n'
            # text5 = 'conversion efficiency: ' + "{:5.3f}".format(conversion) + ' %' + '\n'
    
            # text = text0 + text1 + text2 + text3 + text4 + text5
    
            #ax.text(0.05, 0.2, text, transform=ax.transAxes, fontsize=14,
                    #verticalalignment='top')

            plt.show()
        self.N_0 = param1
        self.k_T = param2
        self.fit_results['simplefit'] = {'fit_func': fit_model[0].label, 
                                          'params' : xopt,
                                          'covariance' :'Not calculated.'}
        
        return param1, param2
    
    def fitfun_dose(self, x, *args):
        """
        This ia a assumed model  for  the  particle  spectrum.
        The function’s fitparameters  are   varied  until  it  fits  the  RCFs  dose values.
        A few helper function from fitfunctions.py are used.

        The results are stored in the Attributes current_dose_fitted , ErrorVector and sseJ

        :param x: the three parameter of the function that are varied during the fitting
        :param args: the x and y values of the RCFs  dose values.
        :return: sse: the computed sum of squared differences. This value will be minimized when the method is called during scipy.minimize.
        """
        xdata = args[0]  # input
        ydata = args[1]  # input
        deltax = args[2]
        deltay = args[3]
        model_fun = args[4]
        P1 = x[0]  # to be optimized
        P2 = x[1]  # to be optimized
        P3 = x[2]  # to be optimized
        
        # calc normed weights, relative on two axis; "diagonal error" => square
        # estimation => fulldeltarelative=((deltay/ydata)^2+(deltax/xdata)^2)^(1/2)="square arround datapoint"
        # square is good approximation, cause each datapoint is represented in
        # kind of statistical "quality"
        # weights are inverse of delta-error: w=1/fulldeltarelative
        # length(xdata) is just for real sseJ, cause optimization is independent
        # of factor in front => minimum is equal minimum times constant,
        # but real sseJ needs this cause (n=2, equal weights, normed)
        # =>2*(0.5*delta1+0.5*delta2)=>gives "weighted real sseJ"
        w = np.power(((deltay / ydata) ** 2 + (deltax / xdata) ** 2), (-1 / 2))
        wnormed=len(xdata)*w /np.sum(w, axis=0)
      
        ELOarray1 = self.ELOarray
        h, b, z = ELOarray1.shape
        for i in range(h):
            if (ELOarray1[i, 0, 0] == 0):
                # prevent division by zero; look into first layer for enery range
                ELOarray1[i, :, :] = 1e-5
        # eneryloss files cut-off at Emax
        ELOarray1 = ELOarray1[ELOarray1[:, 0, 0] <= P3 + 0.001]
        
        # integrate over particle spectrum,weighted by energy-loss in RCF stack.
        integrand = model_fun(params = x, ELO = ELOarray1)
        integrand = np.squeeze(integrand)
        dose = fitfunctions.simpsum_loc(integrand, P3)
        
        FittedCurve_local = np.log(dose)
        self.current_dose_fitted = FittedCurve_local
        self.ErrorVector= FittedCurve_local - ydata
        self.sseJ = np.sum(wnormed * (dose - np.exp(ydata)) ** 2, axis=0)
        sse = np.sum(wnormed * self.ErrorVector ** 2, axis=0)
        return sse

    def fitdeconv(self,N_0, k_T, maxIterations, fit_model=fitfunctions.expfun_fit(), boolean_print=True):
        """
        This method assumed model  for  the  particle  spectrum, the corresponding function is fitfun_dose().
        The approach uses  a  convolution  instead  of  a  deconvolution.
        During this method, RCFs  dose values are calculated. Then the fitfunctions fitfun_dose() parameters  are   varied  until  it  fits  the  RCFs  dose values.

        The start parameters N_0 and k_T for this minimization are the result of the function simplefit().


        :param N_0: start parameter (result of the function simplefit().)
        :param k_T:  start parameter
        :param maxIterations: maximum amount of iterations during the fitting algorithm

        PLOTS:
        -------
        1: deposited energy E_{dep}  X E (MeV)

        2: Proton energy (MeV)       X proton number N per unit energy of 1 MeV
        
        DATA:
        ------
        fit results saved to RCFstack object

        """

        # load variables:
        func = self.fitfun_dose

        Elaser = self.laser.energy
        pulseduration = self.laser.pulseduration
        focusdia = self.laser.focusdia
        last = self.numberOfRCFs

        # Dose: Sum Array with all RCF-images in stack
        ImageArrays = np.empty([self.rcf_list[0].height, self.rcf_list[0].width, last])
        for i in range(last):
            ImageArrays[:, :, i] = self.rcf_list[i].MeV_map_cleaned
        dose = np.squeeze(np.sum(np.sum(ImageArrays, axis=1), axis=0))
        doselog = np.log(dose)

        # DoseError: Sum Array with all RCF-image Errors in stack
        ImageArrayErrors = np.empty([self.rcf_list[0].height, self.rcf_list[0].width, last])
        for i in range(last):
            ImageArrayErrors[:, :, i] = self.rcf_list[i].ErrMeVpx_cleaned
        dose_error = np.squeeze(np.sum(np.sum(ImageArrayErrors, axis=1), axis=0))


        e = self.ELOarray[:, 3, -1]
        peak_position = np.argmax(e)
        E = np.mean(self.ELOarray[peak_position, 0, -1])
        E_error = np.max(abs(self.ELOarray[peak_position, 0, -1] - E))
        Emax_bnd = E + E_error

        E = np.zeros(last)
        E_error = np.zeros(last)

        for frame in range(last):
            peak_position = np.argmax(self.ELOarray[:, 3, frame])
            E[frame] = np.mean(self.ELOarray[peak_position, 0, frame])
            E_error[frame] = abs(self.ELOarray[peak_position, 0, frame] - E[frame])

        E_max = E[-1]
        E_max_low = E[-1] - E_error[-1] - 1

        flag = True
        meanerrorbefore = 0

        # test for 26.44
        # E_max_low = 29.99
        # E_max  = 30
        # Emax_bnd = 30.5
        bnds = ((None, None), (None, None), (E_max_low, Emax_bnd))
        N_start = N_0
        T_start = k_T


        while (flag):
            x0 = np.array([N_start * 1e-12, T_start, E_max])
            if (func == self.fitfun_dose):
                r2 = scipy.optimize.minimize(fun=func, x0=x0, bounds=bnds,
                                             args=(E, doselog, E_error, np.log(dose_error), fit_model[1]), method="Nelder-Mead",
                                             options={'maxiter': maxIterations})
            deconv_estimates = r2.x
            dose_fitted = self.current_dose_fitted
            sdr = self.sseJ
            meanerror = np.mean(abs(np.exp(dose_fitted) - dose) / dose) * 100
            meanerror = np.floor(meanerror * 1e4) / 1e4

            if (meanerror > meanerrorbefore):
                meanerrorbefore = meanerror
                N_start = deconv_estimates[0] * 1e12
                T_start = deconv_estimates[1]
                E_max = deconv_estimates[2]
            else:
                flag = False

        # % fit values
        N_deconv = deconv_estimates[0] * 1e12
        T_deconv = deconv_estimates[1]
        Emax_deconv = deconv_estimates[2]
        dose_fitted_Nelder = self.current_dose_fitted

        if boolean_print:
            fig, ax = plt.subplots()
            ax.plot(E, np.exp(dose_fitted_Nelder), "b")
            ax.plot(E, dose, "rx")
            plt.title("fit deconv, Method: Nelder-Mead \n Parameter: " + str(N_deconv) + ", " + str(T_deconv) + ", " + str(
                Emax_deconv)
                      + "\n dose_fitted: " + str(dose_fitted_Nelder))
            plt.show()

        # for jacobian error estimation we only need 2 of the fitted 3 parameters
        deconv_estimatesJ = np.array([deconv_estimates[0], deconv_estimates[1]])

        # degrees of freedom in the problem; 2 accounts for two fit-parameters
        dof = len(dose) - 2

        # standard deviation of the residuals
        sdr = np.sqrt(sdr / dof)

        jj = jacobianest(Emax_deconv, deconv_estimatesJ, self.ELOarray)
        J = jj[0]
        J = J * 1e-10
        Sigma = (sdr ** 2) / np.dot(np.transpose(J), J) * 1e-20

        # Parameter standard errors
        se = np.transpose(np.sqrt(np.diag(Sigma)))

        # which suggest rough confidence intervalues around
        # the parameters might be...

        deconv_estimates_upJ = deconv_estimatesJ + 2 * se
        deconv_estimates_lowJ = deconv_estimatesJ - 2 * se

        deconv_estimates_up = np.array([deconv_estimates_upJ[0], deconv_estimates_upJ[1], Emax_deconv])
        deconv_estimates_low = np.array([deconv_estimates_lowJ[0], deconv_estimates_lowJ[1], Emax_deconv])

        self.fitfun_dose(deconv_estimates_up, E, doselog, E_error, np.log(dose_error), fit_model[1])
        dose_fitted_up = self.current_dose_fitted

        self.fitfun_dose(deconv_estimates_low, E, doselog, E_error, np.log(dose_error), fit_model[1])
        dose_fitted_low = self.current_dose_fitted

        # fit values conf-interval
        N_deconvup = deconv_estimates_up[0] * 1e12
        T_deconvup = deconv_estimates_up[1]
        N_deconvlow = deconv_estimates_low[0] * 1e12
        T_deconvlow = deconv_estimates_low[1]

        N_deconvErr = ((N_deconv - N_deconvlow) + (N_deconvup - N_deconv)) / 2
        T_deconvErr = ((T_deconv - T_deconvlow) + (T_deconvup - T_deconv)) / 2

        # %find the exponent of the fit parameters
        N_exp = np.floor(np.log10(N_deconv))
        # N_exp = np.floor(np.log(N_deconv))

        N_deconv = N_deconv * 10 ** (-N_exp)
        N_deconvErr = N_deconvErr * 10 ** (-N_exp)

        deconv_estimates = np.array([deconv_estimates[0] * 1e12, deconv_estimates[1], deconv_estimates[2]])

        # Intensity in W/cm^2
        I = Elaser * 2 * np.sqrt(np.log(2)) / (pulseduration * 1e-15 * math.pi * np.sqrt(math.pi) * (
                    focusdia * 1e-6 / (2 * np.sqrt(np.log(2)))) ** 2) * 1e-4
        if boolean_print:
            fig, ax1 = plt.subplots()
            ax1.plot(E, np.exp(dose_fitted_Nelder), 'r--', label='calculated dep. energy from fit')
            ax1.plot(E, np.exp(dose_fitted_low), 'k--', )
            ax1.plot(E, np.exp(dose_fitted_up), 'k--', label='95% confidence band')
    
            ax1.errorbar(E, dose, yerr=dose_error, fmt='o', ecolor='r', label="measured dep. energy in RCF")
            ax1.set_xlim([np.min(E) - 1, np.max(E) + 2])
            ax1.set_ylim([np.min(dose) - 0.8 * np.min(dose), np.max(dose) + 0.8 * np.max(dose)])
            ax1.set_yscale('log')
            plt.xlabel('E (MeV)')
            plt.ylabel('deposited energy E_{dep} (MeV)')
            plt.legend()

            plt.show()

        # calculate conversion efficiency above 4 MeV
        # summarize over all measured data points

        Etotal = 0
        Etotalup = 0
        Etotallow = 0
        N_deconv = N_deconv * 10 ** (N_exp)
        
        if (last != 0):
            intervalpos = np.zeros((last))
            intervalneg = np.zeros((last))

            for i in range(last - 1, -1, -1):
                # generalized number determination
                number    = fit_model[0](E[i],[N_deconv, T_deconv])
                numberup  = fit_model[0](E[i],[N_deconvup, T_deconvup])
                numberlow = fit_model[0](E[i],[N_deconvlow, T_deconvlow])

                if (i != len(E) - 1):  # % avoid problems at end of spectrum
                    intervalpos[i] = np.abs(0.5 * (E[i + 1] - E[i]))
                else:
                    intervalpos[i] = np.abs(0.5 * (E[i] - E[i - 1]))
                if (i != 0):  # % avoid problems at first RCF
                    intervalneg[i] = np.abs(0.5 * (E[i] - E[i - 1]))
                else:
                    intervalneg[i] = np.abs(0.5 * (E[i + 1] - E[i]))

                Ecurrent = number * (intervalneg[i] + intervalpos[i]) * E[i]
                Ecurrentup = numberup * (intervalneg[i] + intervalpos[i]) * E[i]
                Ecurrentlow = numberlow * (intervalneg[i] + intervalpos[i]) * E[i]

                if (E[i] >= 4):
                    Etotal = Etotal + Ecurrent
                    Etotalup = Etotalup + Ecurrentup
                    Etotallow = Etotallow + Ecurrentlow

        Eproton = Etotal * 1e6 * 1.602e-19
        Eprotonup = Etotalup * 1e6 * 1.602e-19
        Eprotonlow = Etotallow * 1e6 * 1.602e-19

        conversion = Eproton / Elaser * 100
        conversionup = Eprotonup / Elaser * 100
        conversionlow = Eprotonlow / Elaser * 100

        conversionErr = ((conversion - conversionlow) + (conversionup - conversion)) / 2

        # %intergrate reversed, cause we want particle-number at cut-off exactly
        lastEnergy = Emax_deconv  # %length(energy)
        restEn = Emax_deconv - np.floor(Emax_deconv)
        energy = np.arange(lastEnergy, restEn - 1, -1)  # %1MeV bins, starting with E_cut_off down close to one

        N = np.zeros(len(energy) - 1)
        Nup = np.zeros(len(energy) - 1)
        Nlow = np.zeros(len(energy) - 1)
        ratio = np.zeros(len(energy) - 1)
        Nmain = np.zeros(len(energy) - 1)
        for i in range(len(energy) - 1):
            
            # f1 = lambda x: N_deconv / x * np.exp(-x / T_deconv)
            # i1 = scipy.integrate.quad(f1, energy[i + 1], energy[i])
            # N[i] = i1[0]
            # f2 = lambda x: N_deconvup / x * np.exp(-x / T_deconvup)
            # i2 = scipy.integrate.quad(f2, energy[i + 1], energy[i])
            # Nup[i] = i2[0]
            # f3 = lambda x: N_deconvlow / x * np.exp(-x / T_deconvlow)
            # i3 = scipy.integrate.quad(f3, energy[i + 1], energy[i])
            # Nlow[i] = i3[0]
            
            
            i1 = scipy.integrate.quad(fit_model[0], energy[i + 1], energy[i], args=([N_deconv, T_deconv]))
            N[i] = i1[0]
            i2 = scipy.integrate.quad(fit_model[0], energy[i + 1], energy[i], args=([N_deconvup, T_deconvup]))
            Nup[i] = i2[0]
            i3 = scipy.integrate.quad(fit_model[0], energy[i + 1], energy[i], args=([N_deconvlow, T_deconvlow]))
            Nlow[i] = i3[0]

        Ntest = 1e10
        ENmax = 10

        while (Ntest > 1e8):
            ENmax = ENmax + 0.1
            # f4 = lambda x: N_deconv / x * np.exp(-x / T_deconv)
            # i4 = scipy.integrate.quad(f4, ENmax - 1, ENmax)
            i4 = scipy.integrate.quad(fit_model[0], ENmax - 1, ENmax, args=([N_deconv, T_deconv]))
            Ntest = i4[0]

        # %we have integrated inversly, flip left-right for x and y vector
        N = np.flipud(N)
        Nup = np.flipud(Nup)
        Nlow = np.flipud(Nlow)
        energy = np.flipud(energy)
        energy = energy[1:] # [0:-1]

        # N = np.where(energy <= 4, 0, N)
        # Nup = np.where(energy <= 4, 0, Nup)
        # Nlow = np.where(energy <= 4, 0, Nlow)

        total = np.sum(N * energy, axis=0)  # %MeV
        totalup = np.sum(Nup * energy, axis=0)  # %MeV
        totallow = np.sum(Nlow * energy, axis=0)  # %MeV

        conv = total * 1e6 * 1.6e-19 / Elaser * 100
        convup = totalup * 1e6 * 1.6e-19 / Elaser * 100
        convlow = totallow * 1e6 * 1.6e-19 / Elaser * 100
        convErr = ((conv - convlow) + (convup - conv)) / 2
        
        # Save data to variable
        data_spectrum ={}
        data_spectrum['Energy / MeV'] = energy
        data_spectrum['Number Particles per MeV'] = N
        data_spectrum['Lower Number Particles per MeV'] = Nlow
        data_spectrum['Higher Number Particles per MeV'] = Nup
        self.spectrum_df = pd.DataFrame(data_spectrum)
        
        publish=True
        if publish == True:
            fig, ax1 = plt.subplots()
            plt.plot(energy, N, 'b', marker='o', label='deconvolved fit')
            plt.plot(energy, Nup, 'k--', label='95% confidence band')
            plt.plot(energy, Nlow, 'k--')
            # %for cut-off visualization
            plt.vlines(x=energy[-1], ymin=0, ymax=N[-1], colors="red", linestyles='dashed', linewidth=3,
                       label='deconved particles')
    
            ax1.set_yscale('log')
            ax1.set_xlim([0, np.max(energy) + 1])
            ax1.set_ylim([np.min(N) * 0.01, 2 * np.max(N)])
            # chenge tick label size
            ax1.xaxis.set_tick_params(labelsize=13)
            ax1.yaxis.set_tick_params(labelsize=13)

            plt.ylabel('(dN/dE) / 1 MeV$^{-1}$', fontsize = 15, weight='bold')
            plt.xlabel('Proton energy / MeV', fontsize = 15, weight='bold' )
            plt.legend(fontsize = 13)
            
        elif boolean_print:
            fig, ax1 = plt.subplots()
            plt.plot(energy, N, 'b', marker='o', label='deconvolved fit')
            plt.plot(energy, Nup, 'k--', label='95% confidence band')
            plt.plot(energy, Nlow, 'k--')
            # %for cut-off visualization
            plt.vlines(x=energy[-1], ymin=0, ymax=N[-1], colors="red", linestyles='dashed', linewidth=3,
                       label='deconved particles')
    
            ax1.set_yscale('log')
            ax1.set_xlim([0, np.max(energy) + 1])
            ax1.set_ylim([np.min(N) * 0.01, 2 * np.max(N)])
            plt.ylabel('proton number N per unit energy of 1 MeV')
            plt.xlabel('Proton energy (MeV)')
            plt.legend()

        t2 = "fit with "+ fit_model[0].label+", weighted with energy deposition \n" 
        t3 = "N_0 = (" + "{:.2f}".format(N_deconv * 10 ** -N_exp) + " \u00B1 " + "{:.2f}".format( N_deconvErr) + ')x10^' + "{:.2f}".format(N_exp) + "\n"
        t4 = "kT = (" + "{:.2f}".format(T_deconv) + " \u00B1 " + "{:.2f}".format(T_deconvErr) + ') MeV + \n' 
        t5 = "Ecut = " + "{:.2f}".format(Emax_deconv), ' MeV \n' 
        t6 = "E expected = " + "{:.2f}".format(ENmax) + ' MeV  (Energy bin with 1e8 p) \n'
        t7 = "Conversion efficiency (> 4 MeV; without aperture): (" + "{:.2f}".format(conv) + " \u00B1 " + "{:.2f}".format(convErr) + "% \n"
        t8 = "Intensity = " + "{:.2e}".format(I) + ' W/cm^2'
        print(t2)
        print(t3)
        print(t4)
        print(t5)
        print(t6)
        print(t7)
        print(t8)
        # ax1.text(0.05, 0.2, t2, transform=ax.transAxes, fontsize=14, verticalalignment='top')
        
        # save results to object
        # self.fit_results['deconv'] = {'fit_func':'dN/dE=N_0/E*exp(-(E/kT_{hot}))', 
        #                                  'N_0':[N_deconv,N_deconvErr* 10 ** +N_exp],
        #                                  'kT' :[T_deconv,T_deconvErr],
        #                                  'Ec' :Emax_deconv}
        # EmaxErr doe not exist so far.
        # The error is assumed to be approximately 2 MeV, since this is a reasonable value
        # for the loss between the last layer with signal and the first layer without a signal.
        EmaxErr = 1
        
        # allocate parameters: 
        xopt = [N_deconv, T_deconv, Emax_deconv]
        cov  = np.zeros((fit_model[0].nrOfParams,fit_model[0].nrOfParams))
        cov[0,0] = N_deconvErr* 10 ** +N_exp
        cov[1,1] = T_deconvErr
        cov[2,2] = EmaxErr
        
        self.fit_results['deconv'] = {'fit_func': fit_model[0].label, 
                                          'params' : xopt,
                                          'covariance' : cov**2}
         
    def fitdeconv_bootstrapped(self,N_0, k_T, maxIterations, fit_model=fitfunctions.expfun_fit()[1],nr_of_samples=100, boolean_print=True):
        """
        This method assumed model  for  the  particle  spectrum, the corresponding function is fitfun_dose().
        The approach uses  a  convolution  instead  of  a  deconvolution.
        During this method, RCFs  dose values are calculated. Then the fitfunctions fitfun_dose() parameters  are   varied  until  it  fits  the  RCFs  dose values.

        The start parameters N_0 and k_T for this minimization are the result of the function simplefit().


        :param N_0: start parameter (result of the function simplefit().)
        :param k_T:  start parameter
        :param maxIterations: maximum amount of iterations during the fitting algorithm

        PLOTS:
        -------
        1: deposited energy E_{dep}  X E (MeV)

        2: Proton energy (MeV)       X proton number N per unit energy of 1 MeV
        
        DATA:
        ------
        fit results saved to RCFstack object

        """

        # load variables:
        func = self.fitfun_dose

        Elaser = self.laser.energy
        pulseduration = self.laser.pulseduration
        focusdia = self.laser.focusdia
        last = self.numberOfRCFs

        # Dose: Sum Array with all RCF-images in stack
        ImageArrays = np.empty([self.rcf_list[0].height, self.rcf_list[0].width, last])
        for i in range(last):
            ImageArrays[:, :, i] = self.rcf_list[i].MeV_map_cleaned
        dose = np.squeeze(np.sum(np.sum(ImageArrays, axis=1), axis=0))
        doselog = np.log(dose)

        # DoseError: Sum Array with all RCF-image Errors in stack
        ImageArrayErrors = np.empty([self.rcf_list[0].height, self.rcf_list[0].width, last])
        for i in range(last):
            ImageArrayErrors[:, :, i] = self.rcf_list[i].ErrMeVpx_cleaned
        dose_error = np.squeeze(np.sum(np.sum(ImageArrayErrors, axis=1), axis=0))


        e = self.ELOarray[:, 3, -1]
        peak_position = np.argmax(e)
        E = np.mean(self.ELOarray[peak_position, 0, -1])
        E_error = np.max(abs(self.ELOarray[peak_position, 0, -1] - E))
        Emax_bnd = E + E_error

        E = np.zeros(last)
        E_error = np.zeros(last)

        for frame in range(last):
            peak_position = np.argmax(self.ELOarray[:, 3, frame])
            E[frame] = np.mean(self.ELOarray[peak_position, 0, frame])
            E_error[frame] = abs(self.ELOarray[peak_position, 0, frame] - E[frame])

        E_max = E[-1]
        E_max_low = E[-1] - E_error[-1] - 1

        # data extraction done, fitting follows. 
        # uncertainty calculation is done with the bootstrapping approach:
        # Step 1 Resample Data:
        Sampled_Data = uncertainty_data(x=E,y=doselog,sigma_x=E_error,sigma_y=np.log(dose_error),number_of_samples=nr_of_samples)
        Sampled_Result = np.zeros((len(Sampled_Data[0][1]), fit_model.nrOfParams))
        # do fits for all data sets 
        for i in range(nr_of_samples+1):
            energies = Sampled_Data[0][:,i]
            doselog = Sampled_Data[1][:,i]
            
            # set errors for fit to zero:
            Esigma = np.ones(E_error.shape)
            Dsigma = np.ones(E_error.shape)
            
            # start fitting
    
            flag = True
            meanerrorbefore = 0
    
            # test for 26.44
            # E_max_low = 29.99
            # E_max  = 30
            # Emax_bnd = 30.5
            bnds = ((None, None), (None, None), (E_max_low, Emax_bnd))
            N_start = N_0
            T_start = k_T
    
            # fit routine: 
            while (flag):
                x0 = np.array([N_start * 1e-12, T_start, E_max])
                if (func == self.fitfun_dose):
                    r2 = scipy.optimize.minimize(fun=func, x0=x0, bounds=bnds,
                                                 args=(energies, doselog, Esigma, Dsigma, fit_model), method="Nelder-Mead",
                                                 options={'maxiter': maxIterations})
                deconv_estimates = r2.x
                dose_fitted = self.current_dose_fitted
                sdr = self.sseJ
                meanerror = np.mean(abs(np.exp(dose_fitted) - dose) / dose) * 100
                meanerror = np.floor(meanerror * 1e4) / 1e4
    
                if (meanerror > meanerrorbefore):
                    meanerrorbefore = meanerror
                    N_start = deconv_estimates[0] * 1e12
                    T_start = deconv_estimates[1]
                    E_max = deconv_estimates[2]
                else:
                    flag = False
    
            # % fit values
            N_deconv = deconv_estimates[0] * 1e12
            T_deconv = deconv_estimates[1]
            Emax_deconv = deconv_estimates[2]
            dose_fitted_Nelder = self.current_dose_fitted
            
            Sampled_Result[i,:] = [N_deconv, T_deconv, Emax_deconv]
    
        
        # descriptive statistics on the sampled results:
        meanValue = np.mean(Sampled_Result,axis=0)
        covariance = np.cov(Sampled_Result,rowvar=False)    
        # save results to object
        self.fit_results['deconv_bootstrap'] = {'fit_func': fit_model.label, 
                                              'params' : meanValue,
                                              'covariance' :covariance}
            

    def linear_deconv(self, Ec_Offset = 1):
        """
               #Calculates number of protons for each (rcf-)layer

               Parameters
               ----------
                 ELOarray : numpy.ndarray of floats with size (numberOfRCFs ,4300,4)
                   a map with energy loss per RCF

                 ImgArray: numpy.ndarray of floats with size (sizeX ,sizeY,numberOfRCFs)
                   a map with MeV per Pixel per RCF

                 rcfconfig : numpy.ndarray of strings with size numberOfRCFs.
                   shows the type of the RCF (H2, E3 etc.) in the order of the stack construction

                Plots:
                ______
                1 : Particle spectrum before deconvolution

                2: Measured energy loss and energy loss of particle spectrum before deconvolution

                3: article spectrum after deconvolution

                4: Number of protons which are stopped in the layers
                           """

        ImgArray = np.empty([self.rcf_list[0].height, self.rcf_list[0].width, self.numberOfRCFs])
        for i in range(self.numberOfRCFs):
            ImgArray[:, :, i] = self.rcf_list[i].MeV_map_cleaned

        szImg = ImgArray.shape
        depEsum = np.zeros((szImg[2]))
        depEsin = np.zeros(szImg[2])
        Ercf = np.zeros(szImg[2])
        Ercfs = np.zeros(szImg[2])
        szElo = self.ELOarray.shape

        #     Größe der Energieintervale in MeV:
        Es = 1e-3

        #     Dec. Energiebereich
        E = np.arange(np.min(self.ELOarray[:, 0, 0]), np.max(self.ELOarray[:, 0, 0]) + Es, Es)
        szE = len(E)

        #     Die Größe der Energieintervalle im EloArray wird angepasst und in einem
        #     entsprechenden Array (eloInt) gespeichert
        eloInt = np.zeros((szE, szElo[2]))

        for i in range(szElo[2]):
            yinterp = np.interp(E, self.ELOarray[:, 0, i], self.ELOarray[:, 3, i])
            eloInt[:, i] = yinterp

        #     Die Protonenergien, die die meiste Energie in den Schichten verlieren, werden berechnet:
        for i in range(szImg[2]):
            depEsin[i] = np.max(eloInt[:, i])
            maxi = np.argmax(eloInt[:, i])
            Ercf[i] = E[maxi]
            depEsum[i] = np.sum(np.sum(ImgArray[:, :, i]))
            # exponential approximation:
            # depEsum[i] = (1.22E11/E[maxi]) *np.exp(-E[maxi]/7.8)
            # istart = np.argwhere(eloInt[:, i] > 0, 1)
            istart = np.array([np.where(eloInt[:, i] > 0)]).min()
            Ercfs[i] = E[istart]

        #    Jeder Schicht wird eine Energiebreite dErcf zugewiesen:
        dErcf = np.zeros((szImg[2]))
        for i in range(szImg[2]):
            if ('H' in self.rcf_list[i].RCFType.name):  # prüfen, ob mit H anfängt oder mit E -> später über RCFtype prüfen
                dErcf[i] = 0.1
                # print(1)
            elif ('E' in self.rcf_list[i].RCFType.name):
                dErcf[i] = 0.165
                # print(2)

        # Jede Schicht bekommt eine "Startenergie" und eine "Endenergie" zugewiesen.
        # Außerdem wird eine Cut-Off-Energie gewählt. Die Teilchenzahl (nop) bei diesen
        # Energien (Ercfs) werden später variiert und zwischen ihnen wird linear
        # interpoliert.
        Ercfs = np.repeat(Ercfs, 2)
        Ercfs = np.sort(Ercfs)
        for i in range(szImg[2]):
            Ercfs[i * 2 + 1] = Ercfs[i * 2] + dErcf[i]

        Ercfs = np.append(Ercfs, Ercfs[-2] + Ec_Offset)
        interpol = 'linear'
        depEsum0 = depEsum

        #     Dec. Anfangsspektrum
        #     Um ein sinnvolles Anfangsspektrum zu erhalten wird angenommen, dass die
        #     deponierte Energie in jedem RCF durch ein gestopptes Proton erzeugt
        #     wurde. Die Teilchenzahlen für alle anderen Energien werden linear
        #     interpoliert. Das so erhaltene Teilchenspektrum überschätzt den Energieverlust
        #     logischerweise.

        nop = depEsum / depEsin / dErcf
        p_in_rcf0 = nop

        #     nop2 enthält die Teilchenzahl bei den Energien Ercfs
        nop2 = np.repeat(nop, 2)
        nop2 = np.append(nop2, 0)

        #     nopInt enthält die Teilchenzahl für jedes Energie in E
        nopInt = np.interp(E, Ercfs, nop2)

        #     Hier wird lediglich NaN in [] umgewandelt. Die NaNs entstehen, wenn der              # Hier entstehen bei python keine nans...
        #   Bereich, in dem interpoliert werden soll, zu groß ist.
        #   Bsp.: interp1(1:3,1:3,1:4,'linear')

        nopInt = nopInt[~np.isnan(nopInt)]
        E = E[~np.isnan(nopInt)]
        eloInt = eloInt[~np.isnan(nopInt), :]  # hier prüfen, dass eloInt zb (size(E) , 7) ist und nicht umgekehrt
        szE = len(E)

        #     Im diesem Plot ist das Anfangsspektrum zu sehen:
        fig, ax = plt.subplots()
        ax.plot(E, nopInt,'ro')
        ax.plot(Ercfs, nop2, "ko")
        plt.title('Particle spectrum before deconvolution')
        plt.ylabel("Number of protons per 1keV")
        plt.xlabel("E / MeV")
        plt.show()

        nopInt0 = nopInt

        #     Der Energieverlust des Anfangsspektrums wird berechnet:
        depEsumi = np.zeros(szImg[2])
        for i in range(szE - 1, -1, -1):
            depEsumi = depEsumi + nopInt[i] * np.squeeze(eloInt[i, :])

        #     Wie bereits erwähnt überschätzt das Anfangsspektrum den Energieverlust.
        #     Dies ist im folgenden Plot zu sehen:
        fig, ax = plt.subplots()
        ax.plot(Ercf, depEsumi, "or")
        ax.plot(Ercf, depEsum0, "ob")
        plt.title('Measured energy loss (blue) and energy loss of particle spectrum before deconvolution (red)')
        plt.ylabel('Deponierte Energie / MeV')
        plt.xlabel('Energie / MeV')
        plt.show()

        #  Start    deconvolution
        # =========================

        Ercfs0 = Ercfs
        dnop = 1e+9
        dnopj = dnop

        idx1 = np.zeros(len(E))
        dErcf_2 = dErcf / 2

        # Achtung! Die Entfaltung ist nicht Eindeutig. Es gibt viele Teilchenspektren die
        # den gleichen Energieverlust in den RCFs aufweisen.
        #
        # Die Teilchenanzahl (nop) bei den Energien (Ercfs) wird nun so lange um
        # dnopj verändert bis der Energieverlust des Teilchenspektrums und der gemessenen
        # Energieverlust übereinstimmen. Die Routine beginnt dabei bei der letzten
        # Schicht (höchsten Energie) und arbeitet sich dann systematisch nach vorn.
        # Falls das Teilchenspektrum den Energieverlust immer noch überschätzt,
        # obowhl die Teilchenanzahl(nop) bei einer Energie(Ercfs) 0 ist wird die
        # Energie (Ercfs) erhöht, sodass die Teilchenzahl zwischen den beiden
        # Schichten vermindert wird.

        for j in range(szImg[2] - 1, -1, -1):
            dnopj = dnop

            while (np.abs(depEsumi[j] / depEsum0[j] - 1) > 0.0001):

                ar = depEsumi[j] / depEsum0[j] - 1
                if depEsumi[j] >= depEsum0[j]:
                    nop[j] = nop[j] - dnopj
                else:
                    nop[j] = nop[j] + dnopj
                    dnopj = dnopj * 0.5

                if (nop[j] < 0):
                    nop[j] = 0
                    Ercfs[2 * j] = Ercfs[2 * j] + Es
                    if (Ercfs[(2 * j)] > Ercfs[2 * j + 1]):
                        break

                nop2 = np.repeat(nop, 2)
                nop2 = np.append(nop2, 0)
                np.count_nonzero(np.eye(4))
                nopInt = np.interp(E, Ercfs, nop2)
                # count = np.count_nonzero(nopInt)
                nopInt = np.nan_to_num(nopInt, nan=0)
                nopInt = np.where(idx1 == 1, 0, nopInt)

                depEsumi[j] = np.sum(np.transpose(nopInt) * np.squeeze(eloInt[:, j]))
                arr = depEsumi[j] / depEsum0[j] - 1

        # In diesem Plot wird der gemessene Energieverlust und der
        # Energieverlust des erhaltenenen Teilchenspektrums verglichen:
        fig, ax = plt.subplots()
        barWidth = 0.4
        ax.bar(Ercf, depEsum0, width=barWidth, color='blue', edgecolor='black', capsize=7, label='measured deposited energy')
        ax.bar(Ercf + barWidth, depEsumi, width=barWidth, color='red', edgecolor='black', capsize=7,
               label='calculated deposited energy')
        plt.ylabel('Deposited Energy / MeV', fontsize = 15, weight='bold')
        plt.xlabel('Energy / MeV', fontsize = 15, weight='bold')
        plt.yscale('log')
        plt.legend(fontsize = 13)
        ax.xaxis.set_tick_params(labelsize=13)
        ax.yaxis.set_tick_params(labelsize=13)

        #plt.title('Measured energy loss (blue) and energy loss of the deconvoluted spectrum (red)')
        plt.show()

        # Dieser Plot zeigt das erhaltene Teilchenspektrum:
        nopInt = nopInt*1000 # convert from keV to MeV
        fig, ax = plt.subplots()
        ax.plot(E, nopInt, color='blue')
        p_in_rcf = np.zeros((szImg[2]))

        # Mit dieser Option gibt man an, ob die Protonen die in einer Schicht
        # gestoppt wurden (case:0) berechnet wird oder die Anzahl der Protonen die Energie
        # in der Schicht verloren haben (case:1)
        integrate_nopInt = 0

        for i in range(szImg[2]):
            if (integrate_nopInt == 0):
                idx = np.nonzero((Ercf[i] - dErcf_2[i] < E) & (Ercf[i] + dErcf_2[i] > E))
            else:
                idx = np.nonzero(Ercf[i] - dErcf_2[i] < E)

            p_in_rcf[i] = np.sum(nopInt[idx])

            x1 = (Ercf[i] - dErcf_2[i])
            y = np.max(nopInt)

            plt.vlines(x1, ymin=0, ymax=y, ls='--', colors="red", linewidth=0.5)

            if integrate_nopInt == 0:
                x2 = (Ercf[i] + dErcf_2[i])
                plt.vlines(x2, ymin=0, ymax=y, colors="red", ls='--', linewidth=0.5)
            else:
                x2 = E[-1]
                plt.vlines(x2, ymin=0, ymax=y, colors="red", ls=':', linewidth=0.5)

        plt.title('Particle spectrum after deconvolution')
        plt.xlabel('Energy / MeV', fontsize = 15, weight='bold')
        plt.ylabel('Number of protons per 1 MeV', fontsize = 15, weight='bold')
        ax.xaxis.set_tick_params(labelsize=13)
        ax.yaxis.set_tick_params(labelsize=13)
        plt.ylim((0, np.max(nopInt)))
        plt.show()

        # % Es wird die Anzahl der Protonen in den Schichten ausgegeben:
        fig, ax = plt.subplots()
        barWidth = 0.75
        ax.bar(Ercf, p_in_rcf, width=barWidth, color='blue', edgecolor='black', capsize=7)
        plt.ylabel('Number of protons', fontsize = 15, weight='bold')
        plt.xlabel('Energy / MeV', fontsize = 15, weight='bold')
        plt.yscale('log')
        ax.xaxis.set_tick_params(labelsize=13)
        ax.yaxis.set_tick_params(labelsize=13)
        # if integrate_nopInt == 0:
        #     plt.title('Number of protons which are stopped in the layers')
        # else:
        #     plt.title('Number of protons which have lost energy in the layers')
        plt.show()
        
        # Save data to variable
        modelfree ={}
        modelfree['Energy / MeV'] = E
        modelfree['Number Particles per MeV'] = nopInt
        # modelfree_df['Lower Number Particles per MeV'] = Nlow
        # modelfree_df['Higher Number Particles per MeV'] = Nup
        self.modelfree_df = pd.DataFrame(modelfree)



    def linear_deconv_with_uncertainties(self, Ec_Offset=1, nr_of_samples=100):
        """
               #Calculates number of protons for each (rcf-)layer
    
               Parameters
               ----------
                 ELOarray : numpy.ndarray of floats with size (numberOfRCFs ,4300,4)
                   a map with energy loss per RCF
    
                 ImgArray: numpy.ndarray of floats with size (sizeX ,sizeY,numberOfRCFs)
                   a map with MeV per Pixel per RCF
    
                 rcfconfig : numpy.ndarray of strings with size numberOfRCFs.
                   shows the type of the RCF (H2, E3 etc.) in the order of the stack construction
    
                Plots:
                ______
                1 : Particle spectrum before deconvolution
    
                2: Measured energy loss and energy loss of particle spectrum before deconvolution
    
                3: article spectrum after deconvolution
    
                4: Number of protons which are stopped in the layers
                           """
    
        ImgArray = np.empty([self.rcf_list[0].height, self.rcf_list[0].width, self.numberOfRCFs])
        ErrArray = np.empty([self.rcf_list[0].height, self.rcf_list[0].width, self.numberOfRCFs])
        for i in range(self.numberOfRCFs):
            ImgArray[:, :, i] = self.rcf_list[i].MeV_map_cleaned
            ErrArray[:, :, i] = self.rcf_list[i].ErrMeVpx_cleaned
    
        szImg = ImgArray.shape
        depEsum = np.zeros((szImg[2]))
        depEsumSigma = np.zeros((szImg[2]))
        depEsin = np.zeros(szImg[2])
        Ercf = np.zeros(szImg[2])
        Ercfs = np.zeros(szImg[2])
        szElo = self.ELOarray.shape
    
        #     Größe der Energieintervale in MeV:
        Es = 1e-3
    
        #     Dec. Energiebereich
        E = np.arange(np.min(self.ELOarray[:, 0, 0]), np.max(self.ELOarray[:, 0, 0]) + Es, Es)
        szE = len(E)
    
        #     Die Größe der Energieintervalle im EloArray wird angepasst und in einem
        #     entsprechenden Array (eloInt) gespeichert
        eloInt = np.zeros((szE, szElo[2]))
    
        for i in range(szElo[2]):
            yinterp = np.interp(E, self.ELOarray[:, 0, i], self.ELOarray[:, 3, i])
            eloInt[:, i] = yinterp
    
        #     Die Protonenergien, die die meiste Energie in den Schichten verlieren, werden berechnet:
        for i in range(szImg[2]):
            depEsin[i] = np.max(eloInt[:, i])
            maxi = np.argmax(eloInt[:, i])
            Ercf[i] = E[maxi]
            depEsum[i] = np.sum(np.sum(ImgArray[:, :, i]))
            depEsumSigma[i] = np.sqrt(np.sum(np.sum(np.power(ErrArray[:, :, i], 2))))
            #exponential approximation:
            # depEsum[i] = (1.22E11/E[maxi]) *np.exp(-E[maxi]/7.8)
            # istart = np.argwhere(eloInt[:, i] > 0, 1)
            istart = np.array([np.where(eloInt[:, i] > 0)]).min()
            Ercfs[i] = E[istart]
    
        #    Jeder Schicht wird eine Energiebreite dErcf zugewiesen:
        dErcf = np.zeros((szImg[2]))
        for i in range(szImg[2]):
            if ('H' in self.rcf_list[i].RCFType.name):  # prüfen, ob mit H anfängt oder mit E -> später über RCFtype prüfen
                dErcf[i] = 0.1
                # print(1)
            elif ('E' in self.rcf_list[i].RCFType.name):
                dErcf[i] = 0.165
                # print(2)
    
        # Jede Schicht bekommt eine "Startenergie" und eine "Endenergie" zugewiesen.
        # Außerdem wird eine Cut-Off-Energie gewählt. Die Teilchenzahl (nop) bei diesen
        # Energien (Ercfs) werden später variiert und zwischen ihnen wird linear
        # interpoliert.
        Ercfs = np.repeat(Ercfs, 2)
        Ercfs = np.sort(Ercfs)
        for i in range(szImg[2]):
            Ercfs[i * 2 + 1] = Ercfs[i * 2] + dErcf[i]
    
        Ercfs = np.append(Ercfs, Ercfs[-2] + Ec_Offset)
        interpol = 'linear'
        depEsum0 = depEsum
    
        # Data set is inflated with additional points due to the GUM MC approach for the uncertainty evaluation
        result = uncertainty_data(x=None,y=depEsum,sigma_x=None,sigma_y=depEsumSigma,number_of_samples=nr_of_samples)
        # result[0] gives the ydata array of relevance
        
        # Empty spectra: 
        Energies = []
        Counts   = []
        
        # Loop over all variants and do the deconvolution:
        for j in range(np.shape(result[0])[1]):
            # print(j)
            depEsum = result[0][:,j]
        
            #     Dec. Anfangsspektrum
            #     Um ein sinnvolles Anfangsspektrum zu erhalten wird angenommen, dass die
            #     deponierte Energie in jedem RCF durch ein gestopptes Proton erzeugt
            #     wurde. Die Teilchenzahlen für alle anderen Energien werden linear
            #     interpoliert. Das so erhaltene Teilchenspektrum überschätzt den Energieverlust
            #     logischerweise.
        
            nop = depEsum / depEsin / dErcf
            p_in_rcf0 = nop
        
            #     nop2 enthält die Teilchenzahl bei den Energien Ercfs
            nop2 = np.repeat(nop, 2)
            nop2 = np.append(nop2, 0)
        
            #     nopInt enthält die Teilchenzahl für jedes Energie in E
            nopInt = np.interp(E, Ercfs, nop2)
        
            #     Hier wird lediglich NaN in [] umgewandelt. Die NaNs entstehen, wenn der              # Hier entstehen bei python keine nans...
            #   Bereich, in dem interpoliert werden soll, zu groß ist.
            #   Bsp.: interp1(1:3,1:3,1:4,'linear')
        
            nopInt = nopInt[~np.isnan(nopInt)]
            E = E[~np.isnan(nopInt)]
            eloInt = eloInt[~np.isnan(nopInt), :]  # hier prüfen, dass eloInt zb (size(E) , 7) ist und nicht umgekehrt
            szE = len(E)
        
            # #     Im diesem Plot ist das Anfangsspektrum zu sehen:
            # fig, ax = plt.subplots()
            # ax.plot(E, nopInt,'ro')
            # ax.plot(Ercfs, nop2, "ko")
            # plt.title('Particle spectrum before deconvolution')
            # plt.ylabel("Number of protons per 1keV")
            # plt.xlabel("E / MeV")
            # plt.show()
        
            nopInt0 = nopInt
        
            #     Der Energieverlust des Anfangsspektrums wird berechnet:
            depEsumi = np.zeros(szImg[2])
            for i in range(szE - 1, -1, -1):
                depEsumi = depEsumi + nopInt[i] * np.squeeze(eloInt[i, :])
        
            # #     Wie bereits erwähnt überschätzt das Anfangsspektrum den Energieverlust.
            # #     Dies ist im folgenden Plot zu sehen:
            # fig, ax = plt.subplots()
            # ax.plot(Ercf, depEsumi, "or")
            # ax.plot(Ercf, depEsum0, "ob")
            # plt.title('Measured energy loss (blue) and energy loss of particle spectrum before deconvolution (red)')
            # plt.ylabel('Deponierte Energie / MeV')
            # plt.xlabel('Energie / MeV')
            # plt.show()
        
            #  Start    deconvolution
            # =========================
        
            Ercfs0 = Ercfs
            dnop = 1e+9
            dnopj = dnop
        
            idx1 = np.zeros(len(E))
            dErcf_2 = dErcf / 2
        
            # Achtung! Die Entfaltung ist nicht Eindeutig. Es gibt viele Teilchenspektren die
            # den gleichen Energieverlust in den RCFs aufweisen.
            #
            # Die Teilchenanzahl (nop) bei den Energien (Ercfs) wird nun so lange um
            # dnopj verändert bis der Energieverlust des Teilchenspektrums und der gemessenen
            # Energieverlust übereinstimmen. Die Routine beginnt dabei bei der letzten
            # Schicht (höchsten Energie) und arbeitet sich dann systematisch nach vorn.
            # Falls das Teilchenspektrum den Energieverlust immer noch überschätzt,
            # obowhl die Teilchenanzahl(nop) bei einer Energie(Ercfs) 0 ist wird die
            # Energie (Ercfs) erhöht, sodass die Teilchenzahl zwischen den beiden
            # Schichten vermindert wird.
        
            for j in range(szImg[2] - 1, -1, -1):
                dnopj = dnop
        
                while (np.abs(depEsumi[j] / depEsum0[j] - 1) > 0.0001):
        
                    ar = depEsumi[j] / depEsum0[j] - 1
                    if depEsumi[j] >= depEsum0[j]:
                        nop[j] = nop[j] - dnopj
                    else:
                        nop[j] = nop[j] + dnopj
                        dnopj = dnopj * 0.5
        
                    if (nop[j] < 0):
                        nop[j] = 0
                        Ercfs[2 * j] = Ercfs[2 * j] + Es
                        if (Ercfs[(2 * j)] > Ercfs[2 * j + 1]):
                            break
        
                    nop2 = np.repeat(nop, 2)
                    nop2 = np.append(nop2, 0)
                    np.count_nonzero(np.eye(4))
                    nopInt = np.interp(E, Ercfs, nop2)
                    # count = np.count_nonzero(nopInt)
                    nopInt = np.nan_to_num(nopInt, nan=0)
                    nopInt = np.where(idx1 == 1, 0, nopInt)
        
                    depEsumi[j] = np.sum(np.transpose(nopInt) * np.squeeze(eloInt[:, j]))
                    arr = depEsumi[j] / depEsum0[j] - 1
        
            # # In diesem Plot wird der gemessene Energieverlust und der
            # # Energieverlust des erhaltenenen Teilchenspektrums verglichen:
            # fig, ax = plt.subplots()
            # barWidth = 0.4
            # ax.bar(Ercf, depEsum0, width=barWidth, color='blue', edgecolor='black', capsize=7, label='Measured Deposited Energy')
            # ax.bar(Ercf + barWidth, depEsumi, width=barWidth, color='red', edgecolor='black', capsize=7,
            #        label='Calculated Deposited Energy')
            # plt.ylabel('Deposited Energy / MeV', fontsize = 15, weight='bold')
            # plt.xlabel('Energy / MeV', fontsize = 15, weight='bold')
            # plt.yscale('log')
            # plt.legend(fontsize = 13)
            # ax.xaxis.set_tick_params(labelsize=13)
            # ax.yaxis.set_tick_params(labelsize=13)
            # #plt.title('Measured energy loss (blue) and energy loss of the deconvoluted spectrum (red)')
            # plt.show()
        
            # Dieser Plot zeigt das erhaltene Teilchenspektrum:
            nopInt = nopInt*1000 # convert from keV to MeV
            # fig, ax = plt.subplots()
            # ax.plot(E, nopInt, color='blue')
            p_in_rcf = np.zeros((szImg[2]))
        
            # Mit dieser Option gibt man an, ob die Protonen die in einer Schicht
            # gestoppt wurden (case:0) berechnet wird oder die Anzahl der Protonen die Energie
            # in der Schicht verloren haben (case:1)
            integrate_nopInt = 0
        
            for i in range(szImg[2]):
                if (integrate_nopInt == 0):
                    idx = np.nonzero((Ercf[i] - dErcf_2[i] < E) & (Ercf[i] + dErcf_2[i] > E))
                else:
                    idx = np.nonzero(Ercf[i] - dErcf_2[i] < E)
        
                p_in_rcf[i] = np.sum(nopInt[idx])
        
                x1 = (Ercf[i] - dErcf_2[i])
                y = np.max(nopInt)
        
                # plt.vlines(x1, ymin=0, ymax=y, ls='--', colors="red", linewidth=0.5)
        
                if integrate_nopInt == 0:
                    x2 = (Ercf[i] + dErcf_2[i])
                    # plt.vlines(x2, ymin=0, ymax=y, colors="red", ls='--', linewidth=0.5)
                else:
                    x2 = E[-1]
                    # plt.vlines(x2, ymin=0, ymax=y, colors="red", ls=':', linewidth=0.5)
        
            # plt.title('Particle spectrum after deconvolution')
            # plt.xlabel('Energy / MeV', fontsize = 15, weight='bold')
            # plt.ylabel('Number of protons per 1 MeV', fontsize = 15, weight='bold')
            # ax.xaxis.set_tick_params(labelsize=13)
            # ax.yaxis.set_tick_params(labelsize=13)
            # plt.ylim((0, np.max(nopInt)))
            # plt.show()
        
            # # % Es wird die Anzahl der Protonen in den Schichten ausgegeben:
            # fig, ax = plt.subplots()
            # barWidth = 0.75
            # ax.bar(Ercf, p_in_rcf, width=barWidth, color='blue', edgecolor='black', capsize=7)
            # plt.ylabel('Number of protons', fontsize = 15, weight='bold')
            # plt.xlabel('Energy / MeV', fontsize = 15, weight='bold')
            # plt.yscale('log')
            # ax.xaxis.set_tick_params(labelsize=13)
            # ax.yaxis.set_tick_params(labelsize=13)
            # # if integrate_nopInt == 0:
            # #     plt.title('Number of protons which are stopped in the layers')
            # # else:
            # #     plt.title('Number of protons which have lost energy in the layers')
            # plt.show()
            
            # adjust calculated data.
            Energies.append(E)
            Counts.append(nopInt)
            
        # Save data to variable
        modelfree ={}
        modelfree['Energy / MeV'] = Energies
        modelfree['Number Particles per MeV'] = Counts
        # modelfree_df['Lower Number Particles per MeV'] = Nlow
        # modelfree_df['Higher Number Particles per MeV'] = Nup
        self.modelfree_df_with_uncertainty = pd.DataFrame(modelfree)
    







