class Scanner:
    """
          A class to represent the Scanner that was used to scan radiated RCFs

          primitive Attributes
          ----------
          name : str
              name of the scanner
          numberOfRCFtypes : int
              number of calibrated RCF types of this scanner

          non-primitive Attributes (Instance Variables, see Constructor):
          ----------
           rcftypes : RCFtype
                        list of calibrated RCF types as RCFtype object

          """
    numberOfRCFtypes = 0
    name = ""


    def __init__(self, numberOfRCFtypes = 0, name="noname", rcftypes=None):
        """
        Constructs all the necessary attributes for the RCF Stack object.

        Parameters
        ----------
         see attributes

        """
        self.numberOfRCFtypes = numberOfRCFtypes
        self.name = name
        if rcftypes is None:
            self.rcftypes_list = []
        else:
            self.rcftypes_list = rcftypes

    def append_rcftypes_list(self, rcftype):
        self.numberOfRCFtypes = self.numberOfRCFtypes + 1
        self.rcftypes_list.append(rcftype)

    def remove_rcftypes_list(self, rcftype):
        self.numberOfRCFtypes = self.numberOfRCFtypes - 1
        self.rcftypes_list.remove(rcftype)
