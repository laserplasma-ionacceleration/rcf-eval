class Laser:
    """
          A class to represent the laser with which the RCF Stack was irradiated

          primitive Attributes
          ----------
          name : str
              name of the laser
          pulseduration : int
              pulseduration of the laser in fs
          focusdiameter : int
               focusdia  of the laser in micrometer
          wavelength : float
                wavelength of the laser micrometer
          energy : int
                energy of the laser in J
          angle : float
                angle (theta) of incidence
          contrast : float
                laser pulse contrast of the laser
          intensity : float
                 the effective intensity of the laser on the target
                 will be computed with method get_intensity()

          """
    energy = 0
    pulseduration = 0
    focusdia = 0
    wavelenght = 0
    name = ""
    contrast = 0.0
    angle = 0.0
    intensity = 0.0
    

    def __init__(self, energy, pulseduration, focusdia, wavelenght, name, contrast, angle):
        """
                Constructs all the necessary attributes for the Laser object.

                Parameters
                ----------
                 see attributes
                    """
        self.energy = energy
        self.pulseduration = pulseduration
        self.focusdia = focusdia
        self.wavelenght = wavelenght
        self.name = name
        self.contrast = contrast
        self.angle = angle


    def get_intensity(self):
        """
        Calculates and returns the effective intensity of the laser on the target.

        Returns
        -------
        intensity : float
            intensity of the laser in W/cm^2

        """
        import numpy as np
        # general case is that the area is elliptic
        # cylindrical symmetry allows to only modify one of the half axis

        irradiated_area = np.pi * (self.focusdia / 2) ** 2 / np.cos(self.angle)
        intensity = 1E23 * self.energy / (irradiated_area * self.pulseduration)
        self.intensity = intensity
        return intensity


