import statistics

import cv2
import numpy as np
import matplotlib.pyplot as plt
import scipy
import skimage
from skimage.color import rgb2hsv
from PIL import Image
from scipy import ndimage
from scipy.signal import find_peaks, peak_widths
from Exceptions import SaturationException

#### Helper Functions ####
def partFW(Ic, a, b, c, booleanb0=True):
    if booleanb0 == False:
        return (1 + (-a + 2*b*c + Ic)/np.sqrt(a**2 -2*a*Ic + 4*b*c*Ic + Ic**2 ))/(2*b)
    elif booleanb0 == True:
        return a*c/ (a-Ic)**2

def parta(a, b, c, Ic, booleanb0=True):
    if booleanb0 == False:
        return (-1 + (a - Ic)/np.sqrt(a**2 -2*a*Ic + 4*b*c*Ic + Ic**2 ))/(2*b)
    elif booleanb0 == True:
        return c*Ic/(a-Ic)**2

def partb( a, b, c, Ic, booleanb0=True):
    if booleanb0 == False:
        return (c * Ic) / (b * np.sqrt(a ** 2 - 2 * a * Ic + 4 * b * c * Ic + Ic ** 2)) - (-a + Ic + np.sqrt(a ** 2 - 2 * a * Ic + 4 * b * c * Ic + Ic ** 2)) / (2 ** b ** 2)
    elif booleanb0 == True:
        return 0
        
def partc (a, b, c, Ic, booleanb0=True):
    if booleanb0 == False:
        return (Ic/(np.sqrt(a**2 -2*a*Ic + 4*b*c*Ic + Ic**2 )))
    elif booleanb0 == True:
        return (Ic / (a-Ic))
        
def colorToDose(a, b, c, Ic, booleanb0=True):
    if booleanb0 == False:
        return (-a + Ic + np.sqrt(a ** 2 - 2 * a * Ic + 4 * b * c * Ic + Ic ** 2)) / (2 * b)
    elif booleanb0 == True:
        return c*Ic/(a - Ic)
        
def DfitDose(Ic, a, b, c, booleanb0=True):
    #if booleanb0 == False:
        return (a * c + b * Ic * (2 * c + Ic)) / ((c + Ic) ** 2)
    #elif booleanb0 == True:
    #    return a*c/(c+Ic)**2
    

def smooth(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.
    
    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal 
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.
    
    input:
        x: the input signal 
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal
        
    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)
    
    see also: 
    
    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter
 
    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    # if x.ndim != 1:
    #     raise ValueError, "smooth only accepts 1 dimension arrays."

    # if x.size < window_len:
    #     raise ValueError, "Input vector needs to be bigger than window size."


    if window_len<3:
        return x


    # if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
    #     raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"


    s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=np.ones(window_len,'d')
    else:
        w=eval('numpy.'+window+'(window_len)')

    y=np.convolve(w/w.sum(),s,mode='valid')
    return y[int((window_len-1)/2):-int(window_len/2)]
 
# def stretch(a, lower_thresh, upper_thresh):
#     r = 255.0/(upper_thresh-lower_thresh+2) # unit of stretching
#     out = np.round(r*np.where(a>=lower_thresh,a-lower_thresh+1,0)).clip(max=255)
#     return out.astype(a.dtype)

####
def closing_and_opening(bin_mask):
    print("closing_and_opening")
    """removes small white points in the black part a binary mask
        and  small black points in the white part a binary mask

           Parameters
           ----------
           bin_mask : numpy.ndarray with size (h,b,1)
               a binary mask containing only 0 or 1

           Returns
           ------
           numpy.ndarray with size (h,b,1)
               a binary mask containing only 0 or 1

           """
    bin_mask = ndimage.binary_opening(bin_mask)
    bin_mask = ndimage.binary_closing(bin_mask)
    return bin_mask

class RCFImage:
    """
                 A class to represent one scanned RCF

                 primitive Attributes
                 ----------
                 filename : str
                     name of the file with the scanned and cleaned RCF
                 filepath : str
                    path to the file with the scanned and cleaned RCF
                 width : int
                     width of the image of the scanned and cleaned RCF in Pixel
                 height : int
                     height of the image of the scanned and cleaned RCF in Pixel
                 colortype : str
                     Colortype of the scanned image (e.g. rgb)
                 XResolution : int
                    Resolution in X-direction of the image
                 YResolution : str
                     Resolution in Y-direction of the image
                 ResolutionUnit : str
                     Unit of the X and Y Resolution
                 maskValue : float
                     MeV-value to distinguish between background and beam.
                 BackgroundMeVMean  : float
                    average Mev Value of all pixels belonging to the computed background
                    will be computed during method removeBackground()


                 non-primitive Attributes (Instance Variables, see Constructor):
                 ----------
                 #
                  OpticalDensity : numpy.ndarray of floats
                  ImageMask : numpy.ndarray of floats with size (h,w)
                        binary mask to distinguish between background and beam
                  stdabwBG : numpy.ndarray of floats with size (3)
                        same as RCFType.RGBsigma
                  RCFType : RCFType
                        the type of the RCF as RCFType Object
                  elossfile : ElossFile
                        the corresponding ElossFile as ElossFile Object
                  scanner : Scanner
                        the corresponding scanner as Scanner Object


                  ImageArray : numpy.ndarray of floats with size (h,w,3)
                        the scanned RCF image in RGB 16bit color code
                  cleanImageArray : numpy array with size (h,w) and dtype=float
                        negated Image of sthe scanned RCF with substracted influence of the film
                        will be filled with values during method counts2OD()
                  masked_ImageArray : numpy.ndarray of floats with size (h,w)
                        a map with MeV per pixel cropped by ImageMask
                        not useful for further computation but for data visualization

                  MeV_map : numpy.ndarray of floats with size (h,w)
                        a map of the whole image with MeV per pixel
                        will be filled with values during method OD2MeV()
                  MeV_map_cleaned : numpy.ndarray of floats with size (h,w)
                        a map with MeV per pixel cropped by ImageMask and with substracted average background MeV-Values


                  ErrMeVpx : numpy.ndarray of floats with size (h,w)
                            a map of the whole image with error pixel while computing MeV per pixel
                            will be filled with values during method OD2MeV()
                  masked_errdose : numpy.ndarray of floats with size (h,w)
                            a map with error pixel while computing MeV per pixel and cropped by ImageMask
                  ErrMeVpx_cleaned : numpy.ndarray of floats with size (h,w)
                            a map with  error pixel cropped by ImageMask and with substracted average background error pixel-Values


                  pinhole : int
                            0: if there was no pinhole detected during method remove_pinhole()
                            1: if there was a pinhole on the given image during method remove_pinhole()


                 """

    filename = ""
    filepath = ""
    width = 0
    height = 0
    colortype = ""
    XResolution = 0
    YResolution = 0
    ResolutionUnit = 0
    maskValue = np.inf
    pinhole = 0
    BackgroundMeVMean = 0.0

    def __init__(self, filename, filepath, width, height, colortype, XResolution, YResolution, ResolutionUnit, ImageArray, OpticalDensity, ImageMask, stdabwBG, RCFType, elossfile, scanner, pinhole=0):
        """
              Constructs all the necessary attributes for the RCF image object.

                  Parameters
                  ----------
                   see attributes

              """
        self.filename = filename
        self.filepath = filepath
        self.width = width
        self.height = height
        self.colortype = colortype
        self.XResolution = XResolution
        self.YResolution = YResolution
        self.ResolutionUnit = ResolutionUnit
        self.ImageArray = ImageArray
        self.OpticalDensity = OpticalDensity
        self.ImageMask = ImageMask
        self.stdabwBG = stdabwBG
        self.RCFType = RCFType
        self.elossfile = elossfile
        self.scanner = scanner
        self.ErrMeVpx = None            #will be filled with values during method OD2MeV()
        self.cleanImageArray = None     #will be filled with values during method counts2OD()
        self.MeV_map = None             #will be filled with values during method OD2MeV()
        self.masked_ImageArray = None  # will be filled with values during cutAtValue()
        self.masked_errdose = None     # will be filled with values during cutAtValue()
        self.pinhole = pinhole           # will be filled with values during method remove_pinhole()
        self.MeV_map_cleaned = None     #will be filled with values during method remove Background
        self.ErrMeVpx_cleaned = None    #will be filled with values during method remove Background



    def counts2OD(self):

        """
        computes a negated Image of the scanned RCF with substracted influence of the film
         The result is stored in attribute cleanImageArray

        Returns
        -------
        cleanImageArray : numpy array with (xpx,ypx) and dtype=float
        negated Image of the scanned RCF with substracted influence of the film

    """
        npixels = 0  # percentage of pixels saturated, if too many are saturated, the fitting algorithm does not converge

        if not self.ImageArray.shape[2] == 3:  # back RGB
            fail = 3  # set if not all RGB Layers are passed.
        else:
            # 65535 must be dark for calibration
            ImageArray = cv2.bitwise_not(self.ImageArray)  # create the negative of the ImageArray

        if self.ImageArray[self.ImageArray > 65535].size:  # check if there pixels in saturation
            try:
                #calculates only the pixel number and divides by total number of pixels
                npixels = sum(sum(self.ImageArray > 65535)) / (self.ImageArray.shape[0] * self.ImageArray.shape[1])
                raise SaturationException()
            except SaturationException:
                print('Too many Pixels are saturated, the fitting algorithm will not converge')

        self.stdabwBG = self.RCFType.RGBsigma
        clean = self.RCFType.RGBclean

        cleanImageArray= np.subtract(ImageArray, clean)
        cleanImageArray = np.clip(cleanImageArray, 0, 65535)
        cleanImageArray = cleanImageArray.astype("uint16")
        self.cleanImageArray = cleanImageArray

        return cleanImageArray

    def OD2MeV(self, ImageArray):
        """
        evaluate the deposited dose per pixel (MeV) from the given cleaned image of the scan, such as cleanImageArray
          The result is stored in attributes MeV_map and ErrMeVpx

        ______
        Parameters
         ImageArray : numpy array with (xpx,ypx) and dtype=float
           cleaned and negated Pixelmap with subtracted influence of the film


       """
        # we need this, cause calibration is done from 0 to 1 color-values
        # plus downward compatiblity, proceting the integer down to a float between 0 and 1
        if ImageArray.dtype == 'uint16':
            ImageArray = skimage.util.img_as_float(ImageArray)
            # this command converts the image to ranges between 0 and 1. 

        # Check again for values out of range..
        # Just to be sure that there are no negative values and values > 1
        ImageArray = np.clip(ImageArray, 0, 1)

        saturationwarning = 0

        # Dataframes got imported before this function is called and gets passed here.
        # The needed parameters are extracted here:
        # 3 fit params:
        a = self.RCFType.a
        b = self.RCFType.b
        c = self.RCFType.c
        # errors for fit parameters:
        deltaa = self.RCFType.deltaa
        deltab = self.RCFType.deltab
        deltac = self.RCFType.deltac
        # saturation, start and stop, limes Dfit:
        satt  = self.RCFType.satt
        start = self.RCFType.start
        stop  = self.RCFType.stop
        limesDfit = self.RCFType.limesDfit
        # load the covariance matrices:
        covR = self.RCFType.covR
        covG = self.RCFType.covG
        covB = self.RCFType.covB
        
        # load the boolean to control whether b is used:
        booleanb0 = self.RCFType.booleanb0

        # Preallocating the image arrays:
        Ic1 = ImageArray[:, :, 0]
        Ic2 = ImageArray[:, :, 1]
        Ic3 = ImageArray[:, :, 2]
        Is  = ImageArray.shape

        # check for saturations
        px = [sum(sum(Ic1 > satt[0])), sum(sum(Ic2 > satt[1])), sum(sum(Ic3 > satt[2]))]
        # average saturation over all channels
        npixels = sum(px) / (3 * Is[0] * Is[1])
        npixelR = px[0] / (Is[0] * Is[1])
        npixelG = px[1] / (Is[0] * Is[1])
        npixelB = px[2] / (Is[0] * Is[1])
        
        self.stdabwBG = self.stdabwBG / 65535

        # TODO: Colorcode -> Exception: Evtl fragen ob weiter fortfahren in GUI?
        try:
            if npixels > 0.01:
                if npixels < 0.05:
                    raise SaturationException()
                elif npixels <= 0.15:
                    raise SaturationException()
                elif npixels > 0.15:
                    raise SaturationException()
        except SaturationException:
            print("({:.2f}".format(npixelR*100) + "; {:.2f}".format(npixelG*100) + "; {:.2f}".format(npixelB*100) + ") % of RGB pxl are saturated! Result may be wrong, maybe delete it?!" )

        # force values to maximum color value for max. dose usable in calibration
        Ic1[Ic1 > satt[0]] = satt[0]
        Ic2[Ic2 > satt[1]] = satt[1]
        Ic3[Ic3 > satt[2]] = satt[2]

        # errors in color-channel are defined by stdabw of backgroud for each pixel
        # pre-calculate dose error from color-values; partial derivatives
        # partials are needed because the gaussian errorpropagation is used.
        partFWR = partFW(a=a[0], b=b[0], c=c[0], Ic=Ic1, booleanb0=booleanb0)
        partFWG = partFW(a=a[1], b=b[1], c=c[1], Ic=Ic2, booleanb0=booleanb0)
        partFWB = partFW(a=a[2], b=b[2], c=c[2], Ic=Ic3, booleanb0=booleanb0)

        partaR = parta(a=a[0], b=b[0], c=c[0], Ic=Ic1, booleanb0=booleanb0)
        partaG = parta(a=a[1], b=b[1], c=c[1], Ic=Ic2, booleanb0=booleanb0)
        partaB = parta(a=a[2], b=b[2], c=c[2], Ic=Ic3, booleanb0=booleanb0)

        partbR = partb(a=a[0], b=b[0], c=c[0], Ic=Ic1, booleanb0=booleanb0)
        partbG = partb(a=a[1], b=b[1], c=c[1], Ic=Ic2, booleanb0=booleanb0)
        partbB = partb(a=a[2], b=b[2], c=c[2], Ic=Ic3, booleanb0=booleanb0)

        partcR = partc(a=a[0], b=b[0], c=c[0], Ic=Ic1, booleanb0=booleanb0)
        partcG = partc(a=a[1], b=b[1], c=c[1], Ic=Ic2, booleanb0=booleanb0)
        partcB = partc(a=a[2], b=b[2], c=c[2], Ic=Ic3, booleanb0=booleanb0)

        # partdR = partd(a[0], b[0], c[0], d[0], Ic1)
        # partdG = partd(a[1], b[1], c[1], d[1], Ic2)
        # partdB = partd(a[2], b[2], c[2], d[2], Ic3)

        # remove inf and NaN from partial derivatives above:
        # no limes needed, cause we define
        # if FW=0 => dose=0 =>deltaDose=0
        partFWR[np.isnan(partFWR) | np.isinf(partFWR)] = 0
        partFWG[np.isnan(partFWG) | np.isinf(partFWG)] = 0
        partFWB[np.isnan(partFWB) | np.isinf(partFWB)] = 0
        partaR[np.isnan(partaR) | np.isinf(partaR)] = 0
        partaG[np.isnan(partaG) | np.isinf(partaG)] = 0
        partaB[np.isnan(partaB) | np.isinf(partaB)] = 0
        if booleanb0 == False:
            partbR[np.isnan(partbR) | np.isinf(partbR)] = 0
            partbG[np.isnan(partbG) | np.isinf(partbG)] = 0
            partbB[np.isnan(partbB) | np.isinf(partbB)] = 0
        partcR[np.isnan(partcR) | np.isinf(partcR)] = 0
        partcG[np.isnan(partcG) | np.isinf(partcG)] = 0
        partcB[np.isnan(partcB) | np.isinf(partcB)] = 0
        # partdR[np.isnan(partdR) | np.isinf(partdR)] = 0
        # partdG[np.isnan(partdG) | np.isinf(partdG)] = 0
        # partdB[np.isnan(partdB) | np.isinf(partdB)] = 0

        # just a temporay array for the calculation of the covariance's influence
        errdoseRtmp = np.array([partaR * partbR, partaR * partcR, partbR * partcR])
        errdoseRtmp = np.array([errdoseRtmp[ii, :, :] * covR[ii] for ii in range(len(covR))])
        errdoseGtmp = np.array([partaG * partbG, partaG * partcG, partbG * partcG])
        errdoseGtmp = np.array([errdoseGtmp[ii, :, :] * covG[ii] for ii in range(len(covG))])
        errdoseBtmp = np.array([partaB * partbB, partaB * partcB, partbB * partcB])
        errdoseBtmp = np.array([errdoseBtmp[ii, :, :] * covB[ii] for ii in range(len(covB))])

        errdoseR = np.sqrt((partFWR * self.stdabwBG[0]) ** 2 + (partaR * deltaa[0]) ** 2 + (partbR * deltab[0]) ** 2 + (
                partcR * deltac[0]) ** 2 + 2 * sum(errdoseRtmp))
        errdoseG = np.sqrt((partFWG * self.stdabwBG[1]) ** 2 + (partaG * deltaa[1]) ** 2 + (partbG * deltab[1]) ** 2 + (
                partcG * deltac[1]) ** 2 + 2 * sum(errdoseGtmp))
        errdoseB = np.sqrt((partFWB * self.stdabwBG[2]) ** 2 + (partaB * deltaa[2]) ** 2 + (partbB * deltab[2]) ** 2 + (
                partcB * deltac[2]) ** 2 + 2 * sum(errdoseBtmp))

        # find color-value-index for color-value == 0
        indxR = np.where(Ic1 == 0)
        indxG = np.where(Ic2 == 0)
        indxB = np.where(Ic3 == 0)

        # convert from color-value to dose
        # Ic1 gets a lot of nans...
        Ic1 = colorToDose(a=a[0], b=b[0], c=c[0], Ic=Ic1, booleanb0=booleanb0)
        Ic2 = colorToDose(a=a[1], b=b[1], c=c[1], Ic=Ic2, booleanb0=booleanb0)
        Ic3 = colorToDose(a=a[2], b=b[2], c=c[2], Ic=Ic3, booleanb0=booleanb0)

        # where color-value is zero, dose has to be zero!
        Ic1[indxR] = 0
        Ic2[indxG] = 0
        Ic3[indxB] = 0

        # set NaN in dose matrix to zero; needs to be done
        # this way cause sometimes FW=0 gives dose=NaN due to 1/0 in calibration
        Ic1[np.isnan(Ic1)] = 0
        Ic2[np.isnan(Ic2)] = 0
        Ic3[np.isnan(Ic3)] = 0

        # calculate weighting matrix for each color channel
        # weighting will be done related to steepness of FW vs dose curve; Dfit is
        # derivative of fitted model and needs a dose value; needs to be done this
        # way, cause of different doses
        DfitRdose1 = DfitDose(a[0], b[0], c[0], Ic1, booleanb0=booleanb0)
        DfitRdose2 = DfitDose(a[0], b[0], c[0], Ic2, booleanb0=booleanb0)
        DfitRdose3 = DfitDose(a[0], b[0], c[0], Ic3, booleanb0=booleanb0)

        DfitGdose1 = DfitDose(a[1], b[1], c[1], Ic1, booleanb0=booleanb0)
        DfitGdose2 = DfitDose(a[1], b[1], c[1], Ic2, booleanb0=booleanb0)
        DfitGdose3 = DfitDose(a[1], b[1], c[1], Ic3, booleanb0=booleanb0)

        DfitBdose1 = DfitDose(a[2], b[2], c[2],  Ic1, booleanb0=booleanb0)
        DfitBdose2 = DfitDose(a[2], b[2], c[2],  Ic2, booleanb0=booleanb0)
        DfitBdose3 = DfitDose(a[2], b[2], c[2],  Ic3, booleanb0=booleanb0)

        # if Dfits are NaN (sometimes if dose is zero; then replace NaN with limes Dfit->0)
        DfitRdose1[np.isnan(DfitRdose1)] = limesDfit[0]
        DfitRdose2[np.isnan(DfitRdose2)] = limesDfit[0]
        DfitRdose3[np.isnan(DfitRdose3)] = limesDfit[0]

        DfitGdose1[np.isnan(DfitGdose1)] = limesDfit[1]
        DfitGdose2[np.isnan(DfitGdose2)] = limesDfit[1]
        DfitGdose3[np.isnan(DfitGdose3)] = limesDfit[1]

        DfitBdose1[np.isnan(DfitBdose1)] = limesDfit[2]
        DfitBdose2[np.isnan(DfitBdose2)] = limesDfit[2]
        DfitBdose3[np.isnan(DfitBdose3)] = limesDfit[2]

        # now look for starting range of values
        #
        # concerning calculated dose, decide if channel is taken into account
        # now look into green, decide if fullfills condition
        # start[0]=> green starts; start[1]=> blue starts; start[2]=> IR starts
        # if red-dose smaller start[0], don't calculate other channels
        ImageArrayDose = Ic1

        DfitRdose1[ImageArrayDose > stop[0]] = 0
        DfitRdose2[ImageArrayDose > stop[0]] = 0
        DfitRdose3[ImageArrayDose > stop[0]] = 0

        DfitGdose1[ImageArrayDose < start[0]] = 0
        DfitGdose2[ImageArrayDose < start[0]] = 0
        DfitGdose3[ImageArrayDose < start[0]] = 0

        # calculate normalized weightings wR wG wB wIR
        wsteigRdose1 = DfitRdose1 / (DfitRdose1 + DfitGdose1)
        wsteigGdose2 = DfitGdose2 / (DfitRdose2 + DfitGdose2)
        # normalized
        wR = (wsteigRdose1) / (wsteigRdose1 + wsteigGdose2)
        wG = (wsteigGdose2) / (wsteigRdose1 + wsteigGdose2)
        # calculate preliminary dose
        ImageArrayDose = wR * Ic1 + wG * Ic2

        # now look into blue, decide if fullfills condition
        DfitBdose1[ImageArrayDose < start[1]] = 0
        DfitBdose2[ImageArrayDose < start[1]] = 0
        DfitBdose3[ImageArrayDose < start[1]] = 0
        # calculate normalized weightings wR wG wB wIR
        wsteigRdose1 = DfitRdose1 / (DfitRdose1 + DfitGdose1 + DfitBdose1)
        wsteigGdose2 = DfitGdose2 / (DfitRdose2 + DfitGdose2 + DfitBdose2)
        wsteigBdose3 = DfitBdose3 / (DfitRdose3 + DfitGdose3 + DfitBdose3)

        # normalized
        wR = (wsteigRdose1) / (wsteigRdose1 + wsteigGdose2 + wsteigBdose3)
        wG = (wsteigGdose2) / (wsteigRdose1 + wsteigGdose2 + wsteigBdose3)
        wB = (wsteigBdose3) / (wsteigRdose1 + wsteigGdose2 + wsteigBdose3)

        # calculate end dose
        ImageArray = wR * Ic1 + wG * Ic2 + wB * Ic3

        # weighted error dose
        errdose = wR * errdoseR + wG * errdoseG + wB * errdoseB

        # set values outside mask to zero; both error and dose; therefore for dose=0 inside
        # mask an error-value exists

        ImageArray = ImageArray * self.RCFType.scale
        ErrMeVpx   = errdose * self.RCFType.scale

        # convert from dose in MeV/px
        res = self.XResolution
        ImageArray = 25.4 ** 2 / res ** 2 * 1e-3 * ImageArray
        ErrMeVpx   = 25.4 ** 2 / res ** 2 * 1e-3 * ErrMeVpx


        self.MeV_map  = ImageArray
        self.ErrMeVpx = ErrMeVpx

    # def create_binary_mask() :
        
    def segmentation(self, im_color):
        """
        Segmentation function that uses the HUE histogram to find proper thresholds.

        Returns
        -------
        None.

        """
        debug_fig_bool = False
        # im_color is the 
        # img = cv2.imread('./LIGHTShot/quellschuss12.tif')
        
        # Format the image array put in: 
        im_color = (im_color[:, :, 0:3]/65535)*255
        im_color = np.clip(im_color, a_min=0, a_max=None)
        im_color = np.array(im_color, dtype='uint8')
        
        h, b = im_color[:,:,0].shape
        

        hsv = cv2.cvtColor(im_color, cv2.COLOR_RGB2HSV)
        
        hue = hsv[:, :, 0]
        
        hist = cv2.calcHist([hue], [0], None, [256], [0, 256]).flatten()
        
        peaks, properties = find_peaks(hist, prominence=hist.max()/2)
        # YO: rel_height is optimal at some value between 0.8 and 0.9?
        widths, width_heights, left_ips, right_ips= peak_widths(hist, peaks, rel_height=0.8)
        
        right_ip = int(right_ips[-1])
        
        dhist = np.diff(hist)
        valley_idx = right_ip + np.argmax(dhist[right_ip:] > 0)
        
        # YO: DBG
        if debug_fig_bool:
            print(valley_idx, right_ips[-1])
        #-YO: DBG
        
        hue = cv2.GaussianBlur(hue, (7, 7), 3)
        _, thres  = cv2.threshold(hue, valley_idx, 255, cv2.THRESH_BINARY)
        
        
        # convert HUE result to binary mask:
        bin_mask = np.zeros((h,b))
        bin_mask[thres==255] = 1 
        # Opening and Closing to reduce noise
        bin_mask = closing_and_opening(bin_mask)
           
        # Dilation to smooth the contour
        iterationen = int(h / 50)
        if debug_fig_bool:
            print("iterationen", iterationen)
        bin_mask = ndimage.binary_dilation(bin_mask, iterations=iterationen)
    
        if debug_fig_bool:
            savefig = True
            plt.figure()
            plt.imshow(hue, cmap='gray')
            plt.axis('off')
            plt.subplots_adjust(0,0,1,1,0,0)
            if savefig:
                plt.savefig('./../Example Plots/Details/Hue_'+self.filename+'.pdf', bbox_inches='tight', pad_inches=0)
            # plt.draw()
            plt.figure()
            plt.plot(hist/1000)
            plt.plot([valley_idx,valley_idx], [0,np.amax(hist/1000)],'--', linewidth = 2)
            plt.tick_params(axis='both',labelsize =13)
            plt.xlim([0,255])
            plt.ylabel('Countrate / 1000', fontsize = 15, weight='bold')
            plt.xlabel('Grayscale', fontsize = 15, weight='bold' )
            if savefig:
                plt.savefig('./../Example Plots/Details/Hist_'+self.filename+'.pdf', bbox_inches='tight', pad_inches=0)
            
            
            plt.figure()
            plt.imshow(thres, cmap='gray')
            plt.axis('off')
            plt.subplots_adjust(0,0,1,1,0,0)
            # plt.draw()
            # plt.pause(0.001)
            plt.figure()
            plt.imshow(bin_mask, cmap='gray')
            plt.axis('off')
            plt.subplots_adjust(0,0,1,1,0,0)
            if savefig:
                plt.savefig('./../Example Plots/Details/BinMask_'+self.filename+'.pdf', bbox_inches='tight', pad_inches=0)
            ## Plot contour on the hue image
            plt.figure()
            plt.imshow(hue, cmap='gray')
            plt.contour(bin_mask,colors='r')
            plt.axis('off')
            plt.subplots_adjust(0,0,1,1,0,0)
            if savefig:
                plt.savefig('./../Example Plots/Details/Contour_'+self.filename+'.pdf', bbox_inches='tight', pad_inches=0)
                plt.close('all')
            
        self.ImageMask = bin_mask
    
        
    def getAutomaticMaskValuefromMeV(self,MeV_map):

        """calculates a proposition for a threshold which distinguishes between background and radiated area in the MeV Map
          The result is stored in attributes ImageMask and  maskValue


               Parameters
               ----------
               MeV_map :  numpy array with (xpx,ypx) and dtype=float
                    Pixelmap with MeV per pixel,

               """

        debug_fig_bool = False
        # if there was a pinhole:
        # remove all pixels which belong to the interpolated pinhole.
        # Otherwise a unintentionally peak will occur in the histogram
        if self.pinhole == 1:
            h, b = MeV_map.shape
            value_pinhole = MeV_map[int(h / 2), int(b / 2)]
            listMevValues = []
            for i in range(h):
                for j in range(b):
                    if (MeV_map[i, j] != value_pinhole):
                        listMevValues.append(MeV_map[i, j])
            MeV_map_without_pinhole = np.array(listMevValues)
        else:
            MeV_map_without_pinhole = MeV_map
            
        # After the setup is done and the hole interpolated, two different recipes are applied. 
        # One ist tailored to HD and one is tailored to EBT Layers. 
        # The behaviour is slightly different since EBT is capable of detecting electron signals as well.
        # This elctron signal is part of an additional low MeV/px peak in the corresponding histogram.
        if 'H' in self.RCFType.name:

            # show the histogram and detect peaks wit a sufficent height and prominence
            ## np.unique makes a problem with the "new" calibration, since there are no duplicates anymore 
            ## in the low energy regime, therefore to many bins are created and the peak detection does not work anymore.
            # unique, counts = np.unique(MeV_map_without_pinhole, return_counts=True)
            counts, binwidth  = np.histogram(MeV_map_without_pinhole, bins=8000)#1500)
            # extract middle value from bin widths, this is then interpreted as the x component calles unique.
            unique = (binwidth[1:]+binwidth[:-1])/2
            
            counts[-int(len(unique)/3):] = 1 # due to signal clipping/saturation a large peak exists at the maximal value and can be cut.
            counts = smooth(counts,window_len=60,window='flat')
            # calc the mean value of all counts
            # this is used to stretch the histogram
            # threshold_counts = np.mean(counts)
            # threshold_unique = unique[counts >= threshold_counts]
            # if 'H' in self.RCFType.name:
            #     # redo histogram  with new boundaries
            #     counts, binwidth  = np.histogram(MeV_map_without_pinhole, bins=800, range=(np.min(threshold_unique),np.max(threshold_unique))) #1500)range=None
            #     counts = smooth(counts,window_len=60,window='flat')
            #     unique = (binwidth[1:]+binwidth[:-1])/2
            #     #print(np.min(threshold_unique),np.max(threshold_unique))
    
            ## testing extrema: 
            #     ___ detection of local minimums and maximums ___
            extrema = np.diff(np.sign(np.diff(counts))).nonzero()[0] + 1               # local min & max
            minimum = (np.diff(np.sign(np.diff(counts))) > 0).nonzero()[0] + 1         # local min
            maximum = (np.diff(np.sign(np.diff(counts))) < 0).nonzero()[0] + 1         # local max
            # +1 due to the fact that diff reduces the original index number
            if debug_fig_bool:
                plt.figure()
                plt.plot(unique, counts, color='grey')
                plt.plot(unique[maximum], counts[maximum], "o", label="max", color='r')
                plt.plot(unique[minimum], counts[minimum], "o", label="min", color='b')
                plt.legend()
                plt.show()
            
            
            if debug_fig_bool:
                plt.figure()
                plt.title('Histogram')
                plt.plot(unique,counts)
                plt.yscale("log")
                plt.show()
            height = np.max(counts) / 10 #2
            #print(height)
            prom = np.max(counts)/500 #500# / 5#None
            peaks, _ = find_peaks(counts, height=height, prominence=prom) #distance=30,
            
            # if no peaks are found, reduce the minimum height for the peak detection
            while (len(peaks) == 0):
                height = height / 2
                prom   = prom / 2
                peaks, _ = find_peaks(counts, height=height,  prominence=prom) #distance=30,
            
            peaks, _ = find_peaks(counts, height=height, prominence=prom)#distance=30, 
            if debug_fig_bool:
                plt.figure()
                plt.title('Peaks')
                plt.plot(counts)
                plt.plot(peaks, counts[peaks], "x")
                plt.show()
    
            # find the peak furthest to the right
            right_peak = unique[peaks[-1]] #-1
            # calculate width of the peak as minimum distance for the following valley.
            # peakoffset = int(peak_widths(counts, [peaks[-1]],rel_height=0.7)[0])
            # if debug_fig_bool:
            #     print('Peakoffset', peakoffset)
            
            # Find the valley next to this peak
            height = np.max(counts) / 50
            if (np.max(counts) > 4e5):
                height = np.max(counts) / 25
            valley, _ = find_peaks(counts, height=(-height,0), distance=30, prominence=prom)
            while (len(valley) == 0):
                height = height / 2
                #prom = prom / 2
                valley, _ = find_peaks(counts, height=-height, distance=30, prominence=prom)
            # A valley can not be on the same position as a peak! 
            valley = minimum
            if debug_fig_bool:
                print('peaks', peaks)
                print('valley', valley)
            # Peaks are determined more precisely, due to the numerical conditions than the valleys, 
            # therefore peaks can be substracted from the valley quantitiy. 
            bool_index = np.isin(valley,peaks)
            valley = valley[np.invert(bool_index)]
            
            if debug_fig_bool:
                plt.figure()
                plt.title('Valleys')
                plt.plot(counts)
                plt.plot(valley, counts[valley], "x")
                plt.show()
    
            # Find the valley right after the detected peak => maskValue
            # delete all valleys left to the detected peak: list a
            a=[]
            for i in range(len(valley)):
                # valleys are because of stability detected with a different method.
                # This implies, that the distance fiven above has to be set here 
                # manually. Defult is the 30 as written above.
                if(unique[valley[i]] < unique[peaks[-1]]):#+peakoffset]):# peakoffset)):
                    a.append(i)
    
            if debug_fig_bool:
                print('peak1', unique[peaks[-1]])
                # print('peak', unique[peaks[-1]+peakoffset])
                print('peaks', right_peak)
                # print('peaks', right_peak + peakoffset)
                print('valle', unique[valley[a]])
                print('vall2', unique[valley])
            
            peaks_new = np.delete(valley, a)
            maskValue = unique[peaks_new[0]]
            # print('MaskValue/MeV', maskValue)
            # minus a little bit to make the mask generous
            maskValue = maskValue#*0.9# - (maskValue * 0.1)
            # maskValue = maskValue - (maskValue * 0.2)
            # maskValue = maskValue + (maskValue * 0.07)
            print('MaskValue/MeV', maskValue)
    
            # Plot fuer paper:
            # unique, counts = np.unique(MeV_map_without_pinhole, return_counts=True)
            if debug_fig_bool:
                plt.figure()
                plt.title('Histogram and maskvalue')
                plt.plot(unique, counts)
                plt.vlines(maskValue, ymin= 0, ymax=np.max(counts), colors="orange")
                plt.ylabel("counts")
                plt.xlabel("MeV value")
                plt.show()
    
            self.maskValue = maskValue
    
            bin_mask = np.where(MeV_map > self.maskValue, 1, 0)
            # clean binary mask, make surrounding generous
            bin_mask = ndimage.binary_opening(bin_mask, iterations=20)
            bin_mask = ndimage.binary_dilation(bin_mask, iterations=20)
            
            if debug_fig_bool:
                plt.figure()
                plt.title('Binary mask')
                plt.imshow(bin_mask)
                plt.show()
    
            self.ImageMask = bin_mask
        # EBT evaluation:
        elif 'E' in self.RCFType.name:
            # show the histogram and detect peaks wit a sufficent height and prominence
            ## np.unique makes a problem with the "new" calibration, since there are no duplicates anymore 
            ## in the low energy regime, therefore to many bins are created and the peak detection does not work anymore.
            # unique, counts = np.unique(MeV_map_without_pinhole, return_counts=True)
            counts, binwidth  = np.histogram(MeV_map_without_pinhole, bins=8000)#1500)
            # extract middle value from bin widths, this is then interpreted as the x component calles unique.
            unique = (binwidth[1:]+binwidth[:-1])/2
            
            counts = smooth(counts,window_len=60,window='flat')
            counts[-int(len(unique)/3):] = 1 # due to signal clipping/saturation a large peak exists at the maximal value and can be cut.
            
            ## testing extrema: 
            #     ___ detection of local minimums and maximums ___
            extrema = np.diff(np.sign(np.diff(counts))).nonzero()[0] + 1               # local min & max
            minimum = (np.diff(np.sign(np.diff(counts))) > 0).nonzero()[0] + 1         # local min
            maximum = (np.diff(np.sign(np.diff(counts))) < 0).nonzero()[0] + 1         # local max
            # +1 due to the fact that diff reduces the original index number
            if debug_fig_bool:
                plt.figure()
                plt.plot(unique, counts, color='grey')
                plt.plot(unique[maximum], counts[maximum], "o", label="max", color='r')
                plt.plot(unique[minimum], counts[minimum], "o", label="min", color='b')
                plt.legend()
                plt.show()
            
            
            if debug_fig_bool:
                plt.figure()
                plt.title('Histogram')
                plt.plot(unique,counts)
                plt.yscale("log")
                plt.show()
            height = np.max(counts) / 2 #4
            #print(height)
            prom = np.max(counts)/200 #500# / 5#None
            peaks, _ = find_peaks(counts, height=height, distance=30, prominence=prom)
            
            # if no peaks are found, reduce the minimum height for the peak detection
            while (len(peaks) == 0):
                height = height / 2
                prom   = prom / 2
                peaks, _ = find_peaks(counts, height=height, distance=30, prominence=prom)
            
            peaks, _ = find_peaks(counts, height=height, distance=30, prominence=prom)
            if debug_fig_bool:
                plt.figure()
                plt.title('Peaks')
                plt.plot(counts)
                plt.plot(peaks, counts[peaks], "x")
                plt.show()
    
            # find the peak furthest to the right
            right_peak = unique[peaks[-1]]
            # calculate width of the peak as minimum distance for the following valley.
            peakoffset = int(peak_widths(counts, [peaks[-1]],rel_height=0.7)[0])
            if debug_fig_bool:
                print('Peakoffset', peakoffset)
            
            # Find the valley next to this peak
            height = np.max(counts) / 50
            if (np.max(counts) > 4e5):
                height = np.max(counts) / 25
            valley, _ = find_peaks(counts, height=(-height,0), distance=30, prominence=prom)
            while (len(valley) == 0):
                height = height / 2
                #prom = prom / 2
                valley, _ = find_peaks(counts, height=-height, distance=30, prominence=prom)
            # A valley can not be on the same position as a peak! 
            valley = minimum
            # Peaks are determined more precisely, due to the numerical conditions than the valleys, 
            # therefore peaks can be substracted from the valley quantitiy. 
            bool_index = np.isin(valley,peaks)
            valley = valley[np.invert(bool_index)]
            
            if debug_fig_bool:
                plt.figure()
                plt.title('Valleys')
                plt.plot(counts)
                plt.plot(valley, counts[valley], "x")
                plt.show()
    
            # Find the valley right after the detected peak => maskValue
            # delete all valleys left to the detected peak: list a
            a=[]
            for i in range(len(valley)):
                # valleys are because of stability detected with a different method.
                # This implies, that the distance fiven above has to be set here 
                # manually. Defult is the 30 as written above.
                if(unique[valley[i]] <= unique[peaks[-1]+peakoffset]):# peakoffset)):
                    a.append(i)
            
            if debug_fig_bool:
                print('peak1', unique[peaks[-1]])
                print('peak', unique[peaks[-1]+peakoffset])
                print('peaks', right_peak)
                print('peaks', right_peak + peakoffset)
                print('valle', unique[valley[a]])
                print('vall2', unique[valley])
            
            peaks_new = np.delete(valley, a)
            maskValue = unique[peaks_new[0]]
            # print('MaskValue/MeV', maskValue)
            # minus a little bit to make the mask generous
            maskValue = maskValue#*0.9# - (maskValue * 0.1)
            # maskValue = maskValue - (maskValue * 0.2)
            # maskValue = maskValue + (maskValue * 0.07)
            print('MaskValue/MeV', maskValue)
    
            # Plot fuer paper:
            # unique, counts = np.unique(MeV_map_without_pinhole, return_counts=True)
            if debug_fig_bool:
                plt.figure()
                plt.title('Histogram and maskvalue')
                plt.plot(unique, counts)
                plt.vlines(maskValue, ymin= 0, ymax=np.max(counts), colors="orange")
                plt.ylabel("counts")
                plt.xlabel("MeV value")
                plt.show()
    
            self.maskValue = maskValue
    
            bin_mask = np.where(MeV_map > self.maskValue, 1, 0)
            # clean binary mask, make surrounding generous
            bin_mask = ndimage.binary_opening(bin_mask, iterations=20)
            bin_mask = ndimage.binary_dilation(bin_mask, iterations=20)
            
            if debug_fig_bool:
                plt.figure()
                plt.title('Binary mask')
                plt.imshow(bin_mask)
                plt.show()
    
            self.ImageMask = bin_mask

    def cutAtValue(self, ImageArray, errdose):
        """
            Takes the MeV-Image and the errdose Array and sets all values lower than the maskValue to zero
             The result is stored in attribut ImageMask

           Parameters
           ----------
           ImageArray : numpy array with (xpx,ypx,) and dtype=float
                map with computed MeV per pixel
           errdose : numpy array with (xpx,ypx,) and dtype=float
                map with computed error per pixel

           """

        self.masked_ImageArray = np.where( self.ImageMask > 0, ImageArray, 0)
        self.masked_errdose = np.where( self.ImageMask > 0, errdose, 0)

        # plt.imshow(self.ImageMask)
        # plt.show()


    def setMaskValue(self, MeV_map, v):
        """
                   Sets the attribute MaskValue to the given value

                  Parameters
                  ----------
                  v : float
                    new value for the attribut MaskValue

        """
        self.maskValue = v
        
        bin_mask = np.where(MeV_map > self.maskValue, 1, 0)
        # clean binary mask, make surrounding generous
        bin_mask = ndimage.binary_opening(bin_mask, iterations=20)
        bin_mask = ndimage.binary_dilation(bin_mask, iterations=20)
        # plt.imshow(bin_mask)
        # plt.show()

        self.ImageMask = bin_mask

    def remove_pinhole(self):
        """
        Checks if there is a pinhole in the given image. If yes, returns the image with interpolated pinhole.
            The pinhole is a nearly white section in the image, possibly surrounded by a draker shadow. Both parts, the white and the shadow are disturbiung.
            The result is stored in attribut imageArray: numpy.ndarray
                   The RGB image as numpy array with possible interpolated pinhole, depending on the second return value:
                self.pinhole : int:
                    0: if there was no pinhole
                    1: if there was a pinhole on the given image

               """

        # get the middle part of the image
        image = self.ImageArray
        h, b, z = image.shape
        # plt.imshow(skimage.util.img_as_ubyte(image))
        # plt.axis('off')
        # plt.show()
        # compute the coordinates of the upper left corner of the rectangle
        start_h = int(h / 2) - 200
        start_b = int(b / 2) - 200
        image_part = image[start_h:start_h + 400, start_b:start_b + 400, :]
        h, b, z = image_part.shape
        # plt.imshow(skimage.util.img_as_ubyte(image_part))
        # plt.axis('off')
        # plt.show()

        # convert to grayscale
        # get the value of the brightest pixel
        # round the value of all pixel to the third decimal place.
        image_gray = cv2.cvtColor(image_part, cv2.COLOR_BGR2GRAY)
        grayscales = image_gray.ravel()
        max_lightness = max(grayscales)
        grayscales = np.around(grayscales, decimals=3)

        # get the distribution of the greyscales
        greyscalevalue, counts = np.unique(grayscales, return_counts=True)

        # if there are no pixels with a grey value higher than 61200 (nearly white), then there is no pinhole.
        if (greyscalevalue[-1] < 61200):
            return image, -1, -1

        # Pinhole is detected, because there are nearly white pixels in the image:
        # -> find position and shape of the pinhole (and the surrounding shadow)

        # store the distribution of the greyscales in a numpy array
        distribution = np.zeros((len(greyscalevalue), 2))
        distribution[:, 0] = greyscalevalue
        distribution[:, 1] = counts

        # find the peaks in the distribution. A minimal height of 20 and a distance of 5 between the peaks is proven to be good
        height = np.max(counts) / 20
        peaks, _ = find_peaks(counts, height=height, distance=5)

        # check if the peak with the lowest grey value is < 70 (very dark). If no, there is no shadow around the pinhole
        # set the threshold for the dark/shadow part of the pinhole either to 70 or to the darkest peak (= shadow)
        peaks_xy = distribution[peaks]
        peaks_x = peaks_xy[:, 0]
        min_peaks_x = peaks_x[0]
        if (min_peaks_x > 70):
            min_peaks_x = 70

        # set the threshold for the very bright part of the pinhole:
        max_lightness = max_lightness - (max_lightness * 0.2)

        # compute a binary mask for the pixel that must be interpolated:
        # every pixel with a grey value > max_lightness: belongs to pinhole
        # every pixel with a grey value <= min_peaks_x: belongs to shadow
        bin_mask_pinhole = np.zeros((h, b))
        for i in range(h):
            for j in range(b):
                if (image_gray[i, j] <= min_peaks_x or (image_gray[i, j] > max_lightness)):
                    bin_mask_pinhole[i, j] = 1

        # remove noise and round (generously) the binary mask
        bin_mask_pinhole = ndimage.binary_opening(bin_mask_pinhole)
        bin_mask_pinhole = ndimage.binary_closing(bin_mask_pinhole)
        bin_mask_pinhole = ndimage.binary_dilation(bin_mask_pinhole, iterations=35)

        # to find the values which replace the values of the pixels of the pinhole:
        # find the median RGB value around the pinhole, i.e. the contour of the binary mask of the pinhole

        # new grayscale image like the binary mask. Unfortunalety, the cv2.findContours can't work with the existing binary mask of the pinhole
        for i in range(h):
            for j in range(b):
                if (bin_mask_pinhole[i, j] == 1):
                    image_gray[i, j] = 255

        # compute contour of the binary mask of the pinhole
        image_color_outline = np.copy(image_part)
        ret, thresh = cv2.threshold(image_gray, 254, 255, 0)
        thresh = skimage.util.img_as_ubyte(thresh)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cv2.drawContours(image_color_outline, contours, -1, (255, 0, 0), 1)

        # find the median RGB value along the contour
        outlineR = []
        outlineB = []
        outlineG = []

        for i in range(h):
            for j in range(b):
                if (np.array_equal(image_color_outline[i, j], [255, 0, 0])):
                    outlineR.append(image_part[i, j, 0])
                    outlineG.append(image_part[i, j, 1])
                    outlineB.append(image_part[i, j, 2])

        MedianR = statistics.median_low(outlineR)
        MedianG = statistics.median_low(outlineG)
        MedianB = statistics.median_low(outlineB)

        # replace the pixels in the binary mask with the computed median RGB values
        for i in range(h):
            for j in range(b):
                if (bin_mask_pinhole[i, j] == 1):
                    image_part[i, j, 0] = MedianR
                    image_part[i, j, 1] = MedianG
                    image_part[i, j, 2] = MedianB

        # replace the former extracted rectangle with the image of the interpolated pinhole
        for i in range(0, 400, 1):
            for j in range(0, 400, 1):
                image[start_h + i, start_b + j] = image_part[i, j]

        # plt.imshow(skimage.util.img_as_ubyte(image))
        # plt.axis('off')
        # plt.show()
        self.pinhole = 1
        self.ImageArray = image


    def removeBackground(self):
        """ computes the average error and MeV of the background pixel and subtract te value from the overall MeV Map.
            All pixel values in the background are set to 0.
            Results are stored in attributes MeV_map_cleaned and ErrMeVpx_cleaned

        """
        backgroundpxl = []
        h,b = self.MeV_map.shape
        for i in range (h):
            for j in range (b):
                if (self.ImageMask[i,j] == False):
                    backgroundpxl.append(self.MeV_map[i,j])

        backgroundpxl = np.asarray(backgroundpxl)
        meanBackground = np.mean(backgroundpxl)
        meanBackground_err = np.std(backgroundpxl,ddof=1) / np.sqrt(backgroundpxl.size)

        self.MeV_map_cleaned = self.MeV_map-meanBackground
        self.ErrMeVpx_cleaned = np.sqrt(self.ErrMeVpx **2 + meanBackground_err**2)

        self.MeV_map_cleaned = np.where(self.MeV_map_cleaned < 0 , 0 , self.MeV_map_cleaned )
        self.ErrMeVpx_cleaned = np.where(self.ErrMeVpx_cleaned < 0, 0, self.ErrMeVpx_cleaned)

        #plt.imshow(self.MeV_map_cleaned)
        #plt.show()
